package Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Files_User;

import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Files_User.Admin.UtilFiles.OpenAdmin;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Files_User.Admin.UtilFiles.SaveAdmin;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Files_User.Client.UtilFiles.OpenClient;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Files_User.Client.UtilFiles.SaveClient;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Files_User.Normal.UtilFiles.OpenNormal;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Files_User.Normal.UtilFiles.SaveNormal;

public class Automatic {
	// El open txt auto est� hecho pero es una tonter�a abrir algo dos veces as�
	// que no lo pongo
	public static void open() {
		OpenAdmin.Json_openAuto();
		OpenClient.Json_openAuto();
		OpenNormal.Json_openAuto();
	}

	public static void save() {
		SaveAdmin.savejsonAuto();
		SaveClient.savejsonAuto();
		SaveNormal.savejsonAuto();
		SaveAdmin.Txt_saveAuto();
		SaveClient.Txt_saveAuto();
		SaveNormal.Txt_saveAuto();
	}
}
