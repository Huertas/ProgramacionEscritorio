package Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Files_User.Client.UtilFiles;

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;

import Framework_Sergio_Huertas_Gisbert.Classes.Settings;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.*;

public class SaveClient {
	public static void save(){
		if(Settings.getInstance().getFile()==0){
			guardarjson_USU(null);
		}else if(Settings.getInstance().getFile()==1){
			generatxt();
		}
	}
	public static void Txt_saveAuto() {
		String PATH = null;
		try {
			PATH = new java.io.File(".").getCanonicalPath()
					+ "/src/Framework_Sergio_Huertas_Gisbert/Modules/Users/Users/Model/Files_User/Client/File/Client.txt";
		} catch (IOException e1) {

			e1.printStackTrace();
		}
		if (Singleton.userclient.size() > 0) {
			try {
				File f;

				f = new File(PATH);

				FileOutputStream fo = new FileOutputStream(f);
				ObjectOutputStream o = new ObjectOutputStream(fo);
				o.writeObject(Singleton.userclient);
				o.close();

			} catch (Exception e) {

			}
		} else {
			File path = new File(PATH);

			path.delete();
		}
	}
	public static void savejsonAuto() {
		String PATH = null;
		try {
			PATH = new java.io.File(".").getCanonicalPath()
					+ "/src/Framework_Sergio_Huertas_Gisbert/Modules/Users/Users/Model/Files_User/Client/File/Client.json";
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			XStream xstreamjson = new XStream(new JettisonMappedXmlDriver());
			xstreamjson.setMode(XStream.NO_REFERENCES);
			xstreamjson.alias("Client", Client.class);

			Gson gson = new Gson();
			String json = gson.toJson(Singleton.userclient);
			FileWriter fileXml = new FileWriter(PATH);
			fileXml.write(json.toString());
			fileXml.close();

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error save json", "Error title", JOptionPane.ERROR_MESSAGE);
		}
	}
	public static void guardarjson_USU(Usuario usu) {
		String PATH;

		try {
			XStream xstreamjson = new XStream(new JettisonMappedXmlDriver());
			xstreamjson.setMode(XStream.NO_REFERENCES);
			xstreamjson.alias("Usuarios", Usuario.class);

			JFileChooser fileChooser = new JFileChooser();

			fileChooser.setAcceptAllFileFilterUsed(false);
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("JSON (*.json)", "json"));

			int seleccion = fileChooser.showSaveDialog(null);

			if (seleccion == JFileChooser.APPROVE_OPTION) {
				File JFC = fileChooser.getSelectedFile();
				PATH = JFC.getAbsolutePath();

				if (!PATH.endsWith(".json")) {
					PATH = PATH + ".json";
				}

				Gson gson = new Gson();
				String json;
				FileWriter fileXml;

				if (usu == null) {
					json = gson.toJson(Singleton.userclient);
				} else {
					json = gson.toJson(usu);
				}

				fileXml = new FileWriter(PATH);
				fileXml.write(json.toString());
				fileXml.close();

				JOptionPane.showMessageDialog(null, "Archivo JSON guardado con �xito", "Archivo JSON",
						JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (HeadlessException | IOException e) {
			JOptionPane.showMessageDialog(null, "Error al grabar el JSON", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public static void generatxt() {//ESTO VA
		String PATH = null;
		try {
			File f;
		
			JFileChooser fileChooser = new JFileChooser();
	
			int seleccion = fileChooser.showSaveDialog(null);
	
			if (seleccion == JFileChooser.APPROVE_OPTION) {
	
				File JFC = fileChooser.getSelectedFile();
			
				PATH = JFC.getAbsolutePath();
			
				PATH = PATH + ".txt";
				
				f = new File(PATH);
			

				FileOutputStream fo = new FileOutputStream(f);
			
				ObjectOutputStream o = new ObjectOutputStream(fo);
			
				o.writeObject(Singleton.userclient);
		
				o.close();
			
				JOptionPane.showMessageDialog(null, "Archivo TXT guardado con exito", "Archivo TXT",
						JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error al grabar el TXT", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

}
