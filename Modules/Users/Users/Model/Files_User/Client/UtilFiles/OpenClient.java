package Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Files_User.Client.UtilFiles;

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;

import Framework_Sergio_Huertas_Gisbert.Classes.Settings;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.*;

public class OpenClient {
	public static void open(){
		if(Settings.getInstance().getFile()==0){
			obri_json();
		}else if(Settings.getInstance().getFile()==1){
			obri_txt();
		}
	}
	public static void Txt_openAuto() {
		String PATH = null;

		try {
			File f;
			PATH = new java.io.File(".").getCanonicalPath()
					+ "/src/Framework_Sergio_Huertas_Gisbert/Modules/Users/Users/Model/Files_User/Client/File/Client.txt";
			f = new File(PATH);

			FileInputStream fi = new FileInputStream(f);
			ObjectInputStream oi = new ObjectInputStream(fi);
			Singleton.userclient = (ArrayList<Client>) oi.readObject();
			oi.close();

		} catch (Exception e) {

		}

	}
	public static void Json_openAuto() {
		String PATH = null;
		Client u = new Client("");

		try {
			XStream xstream = new XStream(new JettisonMappedXmlDriver());
			xstream.setMode(XStream.NO_REFERENCES);

			xstream.alias("Client", Client.class);
			try {
				PATH = new java.io.File(".").getCanonicalPath()
						+ "/src/Framework_Sergio_Huertas_Gisbert/Modules/Users/Users/Model/Files_User/Client/File/Client.json";
			} catch (IOException s) {
				s.printStackTrace();
			}

			Singleton.userclient.clear();
			JsonReader reader = new JsonReader(new FileReader(PATH));
			JsonParser parser = new JsonParser();
			JsonElement root = parser.parse(reader);
			Gson json = new Gson();
			JsonArray list = root.getAsJsonArray();
			for (JsonElement element : list) {
				u = json.fromJson(element, Client.class);
				Singleton.userclient.add(u);
			}

		} catch (Exception e) {
		}

	}
	public static void obri_txt() {//esto VA
    	String PATH = null;
        try {
            File f;
            JFileChooser fileChooser = new JFileChooser();
            int seleccion = fileChooser.showOpenDialog(null);
            if (seleccion == JFileChooser.APPROVE_OPTION) {
                File JFC = fileChooser.getSelectedFile();
                PATH = JFC.getAbsolutePath();
                f = new File(PATH);
                FileInputStream fi=new FileInputStream(f);
    			ObjectInputStream oi=new ObjectInputStream(fi);
    			Singleton.userclient = (ArrayList<Client>)oi.readObject();
    			oi.close();
            }
        } catch (HeadlessException | IOException | ClassNotFoundException e) {
        	JOptionPane.showMessageDialog(null, "Error al leer el TXT", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
	public static void obri_json() {
    	String PATH = null;
    	Client e1=new Client("");
    	
        try {
        	  XStream xstream = new XStream(new JettisonMappedXmlDriver());
	          xstream.setMode(XStream.NO_REFERENCES);
			  xstream.alias("empleafijo", Admin.class);
			  
			  JFileChooser fileChooser = new JFileChooser();
	          int seleccion = fileChooser.showOpenDialog(null);
	          if (seleccion == JFileChooser.APPROVE_OPTION) {
	                File JFC = fileChooser.getSelectedFile();
	                PATH = JFC.getAbsolutePath();
	               
	                Singleton.userclient.clear();
	                //AlistEF.efi = (ArrayList<empleafijo>)xstream.fromXML(new FileReader(PATH)); //NO VA
	                /* TAMPOC VA
	                for (int i = 0; i < size; i++) {
	                	e1 = (empleafijo)xstream.fromXML(new FileReader(PATH));    
	                	AlistEF.efi.add(e1);
	                }*/
	              
	                JsonReader lector = new JsonReader(new FileReader(PATH));
	                JsonParser parseador = new JsonParser();
	                JsonElement raiz = parseador.parse(lector);
	            		  
	            	Gson json = new Gson();
	            	JsonArray lista = raiz.getAsJsonArray();
	            	for (JsonElement elemento : lista) {
	            		e1 = json.fromJson(elemento, Client.class);
	            		Singleton.userclient.add(e1);
	            	}
	          } 
        } catch (Exception e) {
        	JOptionPane.showMessageDialog(null, "Error al leer el JSON", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
