package Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.Order;

import java.util.Comparator;

import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.*;

public class OrderDateBorn implements Comparator<Usuario> {

	public int compare(Usuario u1, Usuario u2) {
		if (u1 instanceof Client) {
			return ((Client) u1).getDborn().compareDate(((Client) u2).getDborn());
		} else {
			return ((Admin) u1).getDborn().compareDate(((Admin) u2).getDborn());
		}
	}

}
