package Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.Order;

import java.util.Comparator;

import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.*;

public class OrderUsername implements Comparator <Usuario>{

	public int compare (Usuario u1, Usuario u2) {
		if(u1.getUsername().compareTo(u2.getUsername())>0)
			return 1;
		if(u1.getUsername().compareTo(u2.getUsername())<0)
			return -1;
		return 0;
	}
}
