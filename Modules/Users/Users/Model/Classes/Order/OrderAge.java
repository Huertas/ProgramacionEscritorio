package Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.Order;

import java.util.Comparator;

import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.*;

public class OrderAge implements Comparator <Usuario>{

	public int compare (Usuario u1, Usuario u2) {
		if(u1 instanceof Client){
		if(((Client)u1).getAge()>((Client)u2).getAge())
			return 1;
		if(((Client)u1).getAge()<((Client)u2).getAge())
			return -1;
		return 0;}
		else{
			if(((Admin)u1).getAge()>((Admin)u2).getAge())
				return 1;
			if(((Admin)u1).getAge()<((Admin)u2).getAge())
				return -1;
			return 0;
		}
	}
}