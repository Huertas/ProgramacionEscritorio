package Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.Order;

import java.util.Comparator;

import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.*;

public class OrderName implements Comparator <Usuario>{

	public int compare (Usuario u1, Usuario u2) {
		if(u1.getName().compareTo(u2.getName())>0)
			return 1;
		if(u1.getName().compareTo(u2.getName())<0)
			return -1;
		return 0;
	}
}
