package Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes;
import java.io.Serializable;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import Framework_Sergio_Huertas_Gisbert.Classes.Fecha;
import Framework_Sergio_Huertas_Gisbert.Classes.Language;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.BLL.BLL_user.*;
import Framework_Sergio_Huertas_Gisbert.Utils.F;
@XStreamAlias("Client")
public class Client extends Usuario implements Comparable<Usuario>,Serializable{
	@XStreamAlias("dborn")
	private Fecha dborn;
	@XStreamAlias("dfirst")
	// private int age;
	private Fecha dfirst;
	@XStreamAlias("phone")
	// private int antiquity;
	private int phone;

	public Client(String DNI, String name, String email, int sex, String username, String pass, Fecha dborn,
			Fecha dfirst, int phone, int karma, String karmar, int numcoment) {
		super(DNI, name, email, sex, username, pass, karma, karmar, numcoment);
		this.dborn = dborn;
		// this.age = age;
		this.dfirst = dfirst;
		// this.antiquity = antiquity;
		this.phone = phone;
	}
	public Client(String DNI){
		super(DNI);
	}

	public Fecha getDborn() {
		return dborn;
	}

	public void setDborn(Fecha dborn) {
		this.dborn = dborn;
	}

	public int getAge() {
		return Fuser.age(dborn);
	}

	/*
	 * public void setAge(int age) { this.age = age; }
	 */

	public Fecha getDfirst() {
		Fecha g=F.setFirstDay(dfirst);
		return g;
	}

	public void setDfirst(Fecha dfirst) {
		this.dfirst = dfirst;
	}

	public int getAntiquity() {
		return Fuser.antiquity(dfirst);
	}

	/*
	 * public void setAntiquity(int antiquity) { this.antiquity = antiquity; }
	 */

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}/*
	// ????????
	public Fecha getDateup() {
		return dborn;
	}
	// ????????
	public void setDateup(Fecha dborn) {
		this.dborn = dborn;
	}*/

	@Override
	public String toString() {
		return Language.getInstance().getClient() + "\n" + Language.getInstance().getBornDay() + " =" + F.getFecha(dborn) + "\n"
				+ Language.getInstance().getAge() + " =" + this.getAge() + "\n" + Language.getInstance().getFirstDay()
				+ " =" + F.getFecha(getDfirst()) + "\n" + Language.getInstance().getAntiquity() + " =" + this.getAntiquity() + "\n"
				+ Language.getInstance().getPhone() + " =" + phone + "\n" + super.toString();
	}

	

}
