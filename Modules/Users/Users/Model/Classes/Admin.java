package Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;
import Framework_Sergio_Huertas_Gisbert.Classes.Fecha;
import Framework_Sergio_Huertas_Gisbert.Classes.Language;
import Framework_Sergio_Huertas_Gisbert.Classes.Settings;
import Framework_Sergio_Huertas_Gisbert.Utils.F;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.BLL.BLL_user.*;
@XStreamAlias("Admin")
public class Admin extends Usuario implements Comparable<Usuario>, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@XStreamAlias("dborn")
	private Fecha dborn;
	// private int age;
	@XStreamAlias("dhire")
	private Fecha dhire;
	// private int antiquity;
	@XStreamAlias("phone")
	private int phone;
	@XStreamAlias("salary")
	private float salary;

	public Admin(String DNI, String name, String email, int sex, String username, String pass, Fecha dborn, Fecha dhire,
			int phone, float salary, int karma, String karmar, int numcoment) {
		super(DNI, name, email, sex, username, pass, karma, karmar, numcoment);
		this.dborn = dborn;
		// this.age = age;
		this.dhire = dhire;
		// this.antiquity=antiquity;
		this.phone = phone;
		this.salary = salary;
	}
	public Admin(String DNI){
		super(DNI);
	}

	public Fecha getDborn() {
		
		return dborn;
	}

	public void setDborn(Fecha dborn) {
		this.dborn = dborn;
	}

	public int getAge() {
		return Fuser.age(dborn);
	}

	/*
	 * public void setAge(int age) { this.age = age; }
	 */

	public Fecha getDhire() {
		return dhire;
	}

	public void setDhire(Fecha dhire) {
		this.dhire = dhire;
	}

	public int getAntiquity() {
		return Fuser.antiquity(dhire);
	}

	/*
	 * public void setAntiquity(Fecha dhire) { this.antiquity=+-96 }
	 */

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public float getSalary() { // no estaba seguro de que pudiese haber alg�n
								// problema, as� que he puesto una variable para
								// evitarme jaleos
		float g = salary;
		g=F.Salary(getKarma(), salary, getDhire());
		g = F.getCoin(g);
		g = F.getDecimalFormat(g);
		return g;
	}

	public String getSalaryCurrency() { // Lo mismo pero con la moneda
		float g = salary;
		String a;
		g = F.getCoin(g);
		g = F.getDecimalFormat(g);
		a = g + " " + Settings.getInstance().getCurrency_config();
		return a;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return Language.getInstance().getAdmin() + "\n" + Language.getInstance().getBornDay() + " =" + F.getFecha(this.getDborn()) + "\n"
				+ Language.getInstance().getAge() + " =" + this.getAge() + "\n" + Language.getInstance().getHireDay()
				+ " =" + F.getFecha(this.getDhire()) + "\n" + Language.getInstance().getAntiquity() + " =" + this.getAntiquity() + "\n"
				+ Language.getInstance().getPhone() + " =" + phone + "\n" + Language.getInstance().getSalary() + " ="
				+ getSalaryCurrency() + "\n" + super.toString();

	}

	

}
