package Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;
import Framework_Sergio_Huertas_Gisbert.Classes.Language;
import Framework_Sergio_Huertas_Gisbert.Utils.F;
@XStreamAlias("Usuario")
public abstract class Usuario implements Comparable<Usuario>, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@XStreamAlias("DNI")
	private String DNI;
	@XStreamAlias("name")
	private String name;
	@XStreamAlias("email")
	private String email;
	@XStreamAlias("sex")
	private int sex;
	@XStreamAlias("username")
	private String username;
	@XStreamAlias("pass")
	private String pass;
	@XStreamAlias("karma")
	private int karma;
	@XStreamAlias("karmar")
	private String karmar; // Representación en forma de rango
	@XStreamAlias("numcoment")
	private int numcoment;

	public Usuario(String DNI, String name, String email, int sex, String username, String pass, int karma,
			String karmar, int numcoment) {
		this.DNI = DNI;
		this.name = name;
		this.email = email;
		this.sex = sex;
		this.username = username;
		this.pass = pass;
		this.karma = karma;
		this.karmar = karmar;
		this.numcoment = numcoment;
	}

	public Usuario(String DNI) {
		this.DNI=DNI;
	}

	public int getKarma() {
		karma=F.Karma(getNumcoment());
		return karma;
	}

	public void setKarma(int karma) {
		this.karma = karma;
	}

	public String getKarmar() {
		karmar=F.Karmar(getKarma());
		return karmar;
	}

	public void setKarmar(String karmar) {
		this.karmar = karmar;
	}

	public int getNumcoment() {
		return numcoment;
	}

	public void setNumcoment(int numcoment) {
		this.numcoment = numcoment;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDNI() {
		return DNI;
	}

	public void setDNI(String dNI) {
		DNI = dNI;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	@Override
	public String toString() {
		return "DNI =" + DNI + "\n" + Language.getInstance().getName() + " =" + name + "\n" + "Email =" + email + "\n"
				+ Language.getInstance().getSex() + " ="
				+ ((sex == 0) ? Language.getInstance().getHombre() : Language.getInstance().getMujer()) + "\n"
				+ Language.getInstance().getUsername() + " =" + username + "\n" + Language.getInstance().getPass()
				+ " =" + pass + "\n" + "karma =" + karma + "\n" + "karmar =" + karmar + "\n"
				+ Language.getInstance().getCommentNumber() + " =" + numcoment;
	}
	public int compareTo(Usuario param) {//para ordenar los empleados por nombre
		if(this.getDNI().compareTo(param.getDNI())>0)
			return 1;
		if(this.getDNI().compareTo(param.getDNI())<0)
			return -1;
		return 0;
	  }
	
	public boolean equals(Object param){
		return getDNI().equals(((Usuario)param).getDNI());
	}

}
