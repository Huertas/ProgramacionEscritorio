package Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.DAO;

import Framework_Sergio_Huertas_Gisbert.Classes.Fecha;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.*;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Utils.*;

public class Fuser_Dummies {

	public static String user() {
		String user = "";
		String[] users = { "vicent", "walter", "joan", "maria", "daniel", "josep", "sara", "pepa", "felipe", "jaume",
				"sergio", "paco", "emilio", "nando", "alfons", "eduard", "atalia", "veronica", "fina", "pepe", "oscar",
				"lorelay", "jim", "lola", "laia", "ismael", "jordi", "ramon", "javier", "nuria", "elena", "beltran",
				"pablo", "juanjo" };

		int position = (int) (Math.random() * 222) % 10;
		user = users[position];
		return user;
	}

	public static String username() {
		String user = "";
		String[] users = { "Sargeras", "Illidan", "Kaelthas", "Archimonde", "Gul'dan", "Tyrion", "Arthas", "Kil'Jaeden",
				"Ysera", "Sylvannas", "Varian", "Kadgar", "Medivh", "Thrall", "Grull", "Jaina", "Kalecgos", "Malygos",
				"Alamuerte", "Alexstrasza", "Cairne", "Kelthuzad", "Syndragosa", "Nozdormu", "Cho'gall", "Xavius",
				"Garrosh", "Vol'Jin", "Uther", "Rexxar", "C'thun", "N'zoth", "Yog'ssaron", "Aegwin" };

		int position = (int) (Math.random() * 222) % 10;
		user = users[position];
		return user;
	}

	public static String pass() {
		String pass = "";
		byte i = 0;
		String[] passwords = { "zxc", "gasr", "lkjbh", "as", "uu", "ccsas", "213awda", "qwerthd", "cxxzs", "jiuyk",
				"sef", "paddda", "xnnc", "nvnvb", "allkn", "dsaaaa", "aas", "turu", "fina", "peuyf", "asdcc", "laaaer",
				"jim", "asdla", "bbca", "lel", "joytdi", "rlkjn", "jaasdr", "nunmba", "pord", "blko", "aankj",
				"youan" };
		while (i < 4) {
			int position = (int) (Math.random() * 222) % 10;
			pass = pass + passwords[position];
			i++;
		}
		return pass;
	}

	public static String DNI() {
		byte i = 0;
		int a =0;
		String DNI = "";
		String NIF_STRING_ASOCIATION = "TRWAGMYFPDXBNJZSQVHLCKE";
		while (i < 8) {
			a = ((int) (Math.random() * 1000)) % 10;
			DNI = DNI + a;
			i++;
		}
		a=((int)(Math.random() * 1000))%23;
		DNI = DNI + NIF_STRING_ASOCIATION.charAt(a);
		return DNI;
	}

	public static String email() {
		String user = "";
		String[] users = { "vicent", "walter", "joan", "maria", "daniel", "josep", "sara", "pepa", "felipe", "jaume",
				"sergio", "paco", "emilio", "nando", "alfons", "eduard", "atalia", "veronica", "fina", "pepe", "oscar",
				"lorelay", "jim", "lola", "laia", "ismael", "jordi", "ramon", "javier", "nuria", "elena", "beltran",
				"pablo", "juanjo" };

		int position = (int) (Math.random() * 222) % 10;
		user = users[position] + "@gmail.com";
		return user;
	}

	public static int sex() {
		return ((int) (Math.random() * 1000)) % 2;
	}

	public static Fecha bornDate() {
		int day = (((int) (Math.random() * 1000)) % 28) + 1;
		int month = (((int) (Math.random() * 1000)) % 12) + 1;
		int year = 0;
		while (year < 1950 || year > 2000) {
			year = 1900 + ((((((int) (Math.random() * 1000)) % 10) % 5) + 5) * 10)
					+ (((int) (Math.random() * 1000)) % 10) + 1;
		}
		Fecha x = new Fecha(day + "/" + month + "/" + year);
		return x;
	}

	public static Fecha hireDate(Fecha x) {
		int day = (((int) (Math.random() * 1000)) % 28) + 1;
		int month = (((int) (Math.random() * 1000)) % 12) + 1;
		int year = 0;
		String b = x + "";
		String[] fechaArray = b.split("/");
		while (year < 1966 || year > 2017) {
			year = (Integer.parseInt(fechaArray[2]) + 16) + (((int) (Math.random() * 1000)) % 10);
		}
		Fecha a = new Fecha(day + "/" + month + "/" + year);
		return a;
	}

	public static Fecha firstDate(Fecha a) {
		String b = a + "";
		String[] fechaArray = b.split("/");
		int day = (((int) (Math.random() * 1000)) % 28) + 1;
		int month = (((int) (Math.random() * 1000)) % 12) + 1;
		int year = 0;
		while (year < 1990 || year > 2016) {
			year = (Integer.parseInt(fechaArray[2]) + 16);// + (((int) (Math.random() * 1000)) % 10);
			if(year<1990){
				year=1990+ (((int) (Math.random() * 1000)) % 17);
			}
		}
		Fecha x = new Fecha(day + "/" + month + "/" + year);
		return x;
	}

	public static int Karma() {
		int a = ((int) (Math.random() * 1000));
		return a;
	}

	public static int numComment() {
		int a = ((int) (Math.random() * 1000));
		return a;
	}

	public static String Karmar() {
		String a = "low";
		return a;
	}

	public static int Phone() {
		int i = 0;
		String a = "";
		while (i < 9) {
			a = a + (((int) (Math.random() * 1000))%10);
			i++;
		}
		i = Integer.parseInt(a);
		return i;
	}

	public static float salary() {
		float salary = 0;
		while(salary<800){
			salary=((int) (Math.random() * 1000))+((int) (Math.random() * 1000))+((int) (Math.random() * 1000));
		}
		salary=salary+((((int) (Math.random() * 1000))%100)/100.0f);
		return salary;
	}

	public static void makedummies_admin() {
		Fecha bornDate;
		int location;
		for (int i = 0; i < 5; i++) { 
			bornDate = bornDate();
			Admin a1 = new Admin(DNI(), user(), email(), sex(), username(), pass(), bornDate, hireDate(bornDate),
					Phone(), salary(), Karma(), Karmar(), numComment());
			location = FindUser.find_adminDNI(a1);
			if (location == -1)
				Singleton.useradmin.add(a1);
		}
	}
	public static void makedummies_client () {
		Fecha bornDate;
		int location;
		for (int i=0; i<5; i++) {
			//Client u = new Client(DNI, name, email, sex, username, pass, dborn, dfirst, phone, karma, karmar,
			//numcoment);
			bornDate=bornDate();
			Client c1 = new Client(DNI(), user(), email(), sex(), username(), pass(), bornDate, firstDate(bornDate), Phone(),
					Karma(), Karmar(), numComment());
			location = FindUser.find_clientDNI(c1);
			if (location == -1)
				Singleton.userclient.add(c1);
		}
	}
	public static void makedummies_normal () {
		int location;
		for (int i=0; i<5; i++) {
			//Normal u = new Normal(DNI, name, email, sex, username, pass, karma, karmar, numcoment);
			Normal u1 = new Normal(DNI(), user(), email(), sex(), username(), pass(), Karma(),
					Karmar(), numComment());
			location = FindUser.find_normalDNI(u1);
			if (location == -1)
				Singleton.usernormal.add(u1);
		}
	}
}
