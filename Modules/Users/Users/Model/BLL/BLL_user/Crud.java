package Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.BLL.BLL_user;

import java.util.Collections;

import javax.swing.JOptionPane;

import Framework_Sergio_Huertas_Gisbert.Classes.Fecha;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.*;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.Order.*;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Files_User.Admin.UtilFiles.OpenAdmin;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Files_User.Admin.UtilFiles.SaveAdmin;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Files_User.Client.UtilFiles.OpenClient;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Files_User.Client.UtilFiles.SaveClient;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Files_User.Normal.UtilFiles.OpenNormal;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Files_User.Normal.UtilFiles.SaveNormal;
import Framework_Sergio_Huertas_Gisbert.Classes.Language;
import Framework_Sergio_Huertas_Gisbert.Utils.F;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Utils.*;

public class Crud {
	public static void create() {
		String[] tipo1 = { Language.getInstance().getAdmin(), "Normal", Language.getInstance().getClient() };
		int sex = 0; // 0 es hombre, 1 es mujer
		int option = 0;
		int location = -1;
		option = F.menubuttons(tipo1, Language.getInstance().getOptionMenu(), Language.getInstance().getMenu());
		if (option == 1) {
			String DNI = Fuser.askDNI();
			Normal a = new Normal(DNI);
			location = FindUser.find_normalDNI(a);
			if (location != -1) {
				F.print(Language.getInstance().getUserAlreadyRegistered(), "Error");
			} else {
				String name = Fuser.askName();
				String email = Fuser.askemail();
				sex = Fuser.askSex();
				String username = Fuser.askUserName();
				String pass = Fuser.askPass();
				int karma = 0;
				String karmar = "low";
				int numcoment = 0;
				Normal u = new Normal(DNI, name, email, sex, username, pass, karma, karmar, numcoment);
				Singleton.usernormal.add(u);
			}
		} else if (option == 2) {
			String DNI = Fuser.askDNI();
			Client a = new Client(DNI);
			location = FindUser.find_clientDNI(a);
			if (location != -1) {
				F.print(Language.getInstance().getUserAlreadyRegistered(), "Error");
			} else {
				String name = Fuser.askName();
				String email = Fuser.askemail();
				sex = Fuser.askSex();
				String username = Fuser.askUserName();
				Fecha dfirst = Fuser.GetDfirst();
				String pass = Fuser.askPass();
				Fecha dborn = Fuser.askBornDate();
				int phone = Fuser.askPhone();
				int karma = 0;
				String karmar = "low";
				int numcoment = 0;

				Client u = new Client(DNI, name, email, sex, username, pass, dborn, dfirst, phone, karma, karmar,
						numcoment);
				Singleton.userclient.add(u);
			}
		} else if (option == 0) {
			String DNI = Fuser.askDNI();
			Admin a = new Admin(DNI);
			location = FindUser.find_adminDNI(a);
			if (location != -1) {
				F.print(Language.getInstance().getUserAlreadyRegistered(), "Error");
			} else {
				String name = Fuser.askName();
				String email = Fuser.askemail();
				sex = Fuser.askSex();
				String username = Fuser.askUserName();
				String pass = Fuser.askPass();
				Fecha dborn = Fuser.askBornDate();
				Fecha dhire = Fuser.askHireDate(dborn);
				int phone = Fuser.askPhone();
				float salary = Fuser.askSalary();
				int karma = 0;
				String karmar = "low";
				int numcoment = 0;
				Admin u = new Admin(DNI, name, email, sex, username, pass, dborn, dhire, phone, salary, karma, karmar,
						numcoment);
				Singleton.useradmin.add(u);
			}
		}
	}

	public static void read() {
		String cad = "";
		String[] tipo1 = { Language.getInstance().getAdmin(), "Normal", Language.getInstance().getClient() };
		int option = F.menubuttons(tipo1, Language.getInstance().getOptionMenu(), Language.getInstance().getMenu());
		int menu = 0;
		String[] tipo2 = { Language.getInstance().getAll(), Language.getInstance().getJustOne() };
		int location = -1;
		if (option == 0) {
			if (Singleton.useradmin.isEmpty()) {
				F.print(Language.getInstance().getNoUsersRegistered(), "Error");
			} else {
				menu = F.menubuttons(tipo2, Language.getInstance().getChoose(), "Menu");
				switch (menu) {
				case 0:
					for (int i = 0; i < Singleton.useradmin.size(); i++) {
						cad = cad + (Singleton.useradmin.get(i).toString());
						JOptionPane.showMessageDialog(null, cad);
						cad = "";
					}
					break;
				case 1:
					location = -1;
					Admin a = FindUser.IDadmin();
					if (a != null) {
						location = FindUser.find_adminDNI(a);
						if (location != -1) {
							a = Singleton.useradmin.get(location);
							JOptionPane.showMessageDialog(null, a.toString());
						} else {
							F.print(Language.getInstance().getUserNotFound(), "Error");
						}
					}
					break;
				}
			}

		} else if (option == 1) {

			if (Singleton.usernormal.isEmpty()) {
				F.print(Language.getInstance().getNoUsersRegistered(), "Error");
			} else {
				menu = F.menubuttons(tipo2, Language.getInstance().getChoose(), "Menu");
				switch (menu) {
				case 0:
					for (int i = 0; i < Singleton.usernormal.size(); i++) {
						cad = cad + (Singleton.usernormal.get(i).toString());
						JOptionPane.showMessageDialog(null, cad);
						cad = "";
					}
					break;
				case 1:
					location = -1;
					Normal a = FindUser.IDNormal();
					if (a != null) {
						location = FindUser.find_normalDNI(a);
						if (location != -1) {
							a = Singleton.usernormal.get(location);
							JOptionPane.showMessageDialog(null, a.toString());
						} else {
							F.print(Language.getInstance().getUserNotFound(), "Error");
						}
					}
					break;
				}
			}

		} else if (option == 2) {

			if (Singleton.userclient.isEmpty()) {
				F.print(Language.getInstance().getNoUsersRegistered(), "Error");
			} else {
				menu = F.menubuttons(tipo2, Language.getInstance().getChoose(), Language.getInstance().getMenu());
				switch (menu) {
				case 0:
					for (int i = 0; i < Singleton.userclient.size(); i++) {
						cad = cad + (Singleton.userclient.get(i).toString());
						JOptionPane.showMessageDialog(null, cad);
						cad = "";
					}
					break;
				case 1:
					location = -1;
					Client a = FindUser.IDclient();
					if (a != null) {
						location = FindUser.find_clientDNI(a);
						if (location != -1) {
							a = Singleton.userclient.get(location);
							JOptionPane.showMessageDialog(null, a.toString());
						} else {
							F.print(Language.getInstance().getUserNotFound(), "Error");
						}
					}
					break;
				}
			}

		}
	}

	public static void update() {
		int location = -1;
		int location2 = -1;
		String[] tipo = { Language.getInstance().getAdmin(), "Normal", Language.getInstance().getClient() };
		int option = F.menubuttons(tipo, Language.getInstance().getAskUpdate(), Language.getInstance().getMenu());
		if (option == 0) {
			if (Singleton.useradmin.isEmpty()) {
				F.print(Language.getInstance().getNoUsersRegistered(), "Error");
			} else {
				// String DNI = Fuser.askDNI();
				// Admin u = new Admin(DNI);
				Admin u = FindUser.IDadmin();
				if (u != null) {
					location = FindUser.find_adminDNI(u);
					if (location != -1) {
						u = Singleton.useradmin.get(location);
						String[] tipo1 = { "Dni", Language.getInstance().getName(), "Email",
								Language.getInstance().getUsername(), Language.getInstance().getPass(),
								Language.getInstance().getBornDay(), Language.getInstance().getHireDay(),
								Language.getInstance().getPhone(), Language.getInstance().getSalary(),
								Language.getInstance().getCommentNumber() };
						option = F.menubuttons(tipo1, Language.getInstance().getAskUpdate(),
								Language.getInstance().getUpdateUser());
						switch (option) {
						case 0:
							location2 = -1;
							Admin a = new Admin(Fuser.askDNI());
							if (a != null) {
								location2 = FindUser.find_adminDNI(a);
								if (location2 != -1) {
									JOptionPane.showMessageDialog(null,
											Language.getInstance().getUserAlreadyRegistered());
								} else {
									u.setDNI(a.getDNI());
								}
							}
							break;
						case 1:
							u.setName(Fuser.askName());
							break;
						case 2:
							u.setEmail(Fuser.askemail());
							break;
						case 3:
							u.setUsername(Fuser.askUserName());
							break;
						case 4:
							u.setPass(Fuser.askPass());
							break;
						case 5:
							((Admin) u).setDborn(Fuser.askBornDate());
							break;
						case 6:
							((Admin) u).setDhire(Fuser.askHireDate(((Admin) u).getDborn()));
							// ((Admin) u).setAntiquity(((Admin) u).getDhire());
							break;
						case 7:
							((Admin) u).setPhone(Fuser.askPhone());
							break;
						case 8:
							((Admin) u).setSalary(Fuser.askSalary());
							break;
						default:
							break;
						}
						Singleton.useradmin.set(location, u);
					} else {
						F.print("Error", "Error");
					}
				}
			}

		} else if (option == 2) {
			if (Singleton.userclient.isEmpty()) {
				F.print(Language.getInstance().getNoUsersRegistered(), "Error");
			} else {
				Client u = FindUser.IDclient();
				if (u != null) {
					location = FindUser.find_clientDNI(u);
					if (location != -1) {
						u = Singleton.userclient.get(location);
						String[] tipo1 = { "Email", Language.getInstance().getUsername(),
								Language.getInstance().getPass(), Language.getInstance().getPhone() };
						option = F.menubuttons(tipo1, Language.getInstance().getAskUpdate(),
								Language.getInstance().getUpdateUser());
						switch (option) {
						case 0:
							u.setEmail(Fuser.askemail());
							break;
						case 1:
							u.setUsername(Fuser.askUserName());
							break;
						case 2:
							u.setPass(Fuser.askPass());
							break;
						case 3:
							((Client) u).setPhone(Fuser.askPhone());
							break;
						default:
							break;
						}
						Singleton.userclient.set(location, u);
					}
				}
			}
		} else if (option == 1) {
			if (Singleton.usernormal.isEmpty()) {
				F.print(Language.getInstance().getNoUsersRegistered(), "Error");
			} else {
				Normal u = FindUser.IDNormal();
				if (u != null) {
					location = FindUser.find_normalDNI(u);
					if (location != -1) {
						u = Singleton.usernormal.get(location);
						String[] tipo1 = { "Email", Language.getInstance().getUsername(),
								Language.getInstance().getPass() };
						option = F.menubuttons(tipo1, Language.getInstance().getAskUpdate(),
								Language.getInstance().getUpdateUser());
						switch (option) {
						case 0:
							u.setEmail(Fuser.askemail());
							break;
						case 1:
							u.setUsername(Fuser.askUserName());
							break;
						case 2:
							u.setPass(Fuser.askPass());
							break;
						default:
							break;
						}
						Singleton.usernormal.set(location, u);
					}
				}
			}
		}
	}

	public static void delete() {
		String[] tipo1 = { Language.getInstance().getAdmin(), "Normal", Language.getInstance().getClient() };
		int option = F.menubuttons(tipo1, Language.getInstance().getAskDelete(), Language.getInstance().getMenu());
		int location = -1;
		if (option == 0) {
			if (Singleton.useradmin.isEmpty()) {
				F.print(Language.getInstance().getNoUsersRegistered(), "Error");
			} else {
				location = -1;
				Admin b = FindUser.IDadmin();
				if (b != null) {
					location = FindUser.find_adminDNI(b);
					if (location != -1) {
						Singleton.useradmin.remove(location);
					} else {
						F.print(Language.getInstance().getUserNotFound(), "Error");
					}
				}
			}
		} else if (option == 2) {
			if (Singleton.userclient.isEmpty()) {
				F.print(Language.getInstance().getNoUsersRegistered(), "Error");
			} else {
				location = -1;
				Client b = FindUser.IDclient();
				if (b != null) {
					location = FindUser.find_clientDNI(b);
					if (location != -1) {
						Singleton.useradmin.remove(location);
					} else {
						F.print(Language.getInstance().getUserNotFound(), "Error");
					}
				}
			}
		} else if (option == 1) {
			if (Singleton.usernormal.isEmpty()) {
				F.print(Language.getInstance().getNoUsersRegistered(), "Error");
			} else {
				location = -1;
				Normal b = FindUser.IDNormal();
				if (b != null) {
					location = FindUser.find_normalDNI(b);
					if (location != -1) {
						Singleton.useradmin.remove(location);
					} else {
						F.print(Language.getInstance().getUserNotFound(), "Error");
					}
				}
			}
		}
	}

	public static void order() {
		String[] tipo1 = { Language.getInstance().getAdmin(), "Normal", Language.getInstance().getClient() };
		String[] options = { "DNI", Language.getInstance().getName(), Language.getInstance().getUsername(),
				Language.getInstance().getAge(), Language.getInstance().getBornDay() };
		String[] optionsNormal = { "DNI", Language.getInstance().getName(), Language.getInstance().getUsername() };
		int option = F.menubuttons(tipo1, Language.getInstance().getAskWhatToOrder(), Language.getInstance().getMenu());
		if (option == 0) {
			if (Singleton.useradmin.isEmpty()) {
				F.print(Language.getInstance().getNoUsersRegistered(), "Error");
			} else {
				option = F.menubuttons(options, Language.getInstance().getHow(), Language.getInstance().getMenu());
				switch (option) {
				case 0:
					Collections.sort(Singleton.useradmin);
					break;
				case 1:
					Collections.sort(Singleton.useradmin, new OrderName());
					break;
				case 2:
					Collections.sort(Singleton.useradmin, new OrderUsername());
					break;
				case 3:
					Collections.sort(Singleton.useradmin, new OrderAge());
					break;
				case 4:
					Collections.sort(Singleton.useradmin, new OrderDateBorn());
					break;
				}
			}
		} else if (option == 2) {
			if (Singleton.userclient.isEmpty()) {
				F.print(Language.getInstance().getNoUsersRegistered(), "Error");
			} else {
				option = F.menubuttons(options, Language.getInstance().getHow(), Language.getInstance().getMenu());
				switch (option) {
				case 0:
					Collections.sort(Singleton.userclient);
					break;
				case 1:
					Collections.sort(Singleton.userclient, new OrderName());
					break;
				case 2:
					Collections.sort(Singleton.userclient, new OrderUsername());
					break;
				case 3:
					Collections.sort(Singleton.userclient, new OrderAge());
					break;
				case 4:
					Collections.sort(Singleton.userclient, new OrderDateBorn());
					break;
				}
			}
		} else if (option == 1) {
			if (Singleton.usernormal.isEmpty()) {
				F.print(Language.getInstance().getNoUsersRegistered(), "Error");
			} else {
				option = F.menubuttons(optionsNormal, Language.getInstance().getHow(),
						Language.getInstance().getMenu());
				switch (option) {
				case 0:
					Collections.sort(Singleton.usernormal);
					break;
				case 1:
					Collections.sort(Singleton.usernormal, new OrderName());
					break;
				case 2:
					Collections.sort(Singleton.usernormal, new OrderUsername());
					break;
				case 3:
					Collections.sort(Singleton.usernormal, new OrderAge());
					break;
				case 4:
					Collections.sort(Singleton.usernormal, new OrderDateBorn());
					break;
				}
			}
		}
	}

	public static void save() {
		String[] tipo1 = { Language.getInstance().getAdmin(), "Normal", Language.getInstance().getClient() };
		int option = F.menubuttons(tipo1, Language.getInstance().getAskSave(), Language.getInstance().getMenu());
		if (option == 0) {
			SaveAdmin.save();
		} else if (option == 1) {
			SaveNormal.save();
		} else if (option == 2) {
			SaveClient.save();
		}
	}

	public static void open() {
		String[] tipo1 = { Language.getInstance().getAdmin(), "Normal", Language.getInstance().getClient() };
		int option = F.menubuttons(tipo1, Language.getInstance().getAskOpen(), Language.getInstance().getMenu());
		if (option == 0) {
			OpenAdmin.open();
		} else if (option == 1) {
			OpenNormal.open();
		} else if (option == 2) {
			OpenClient.open();
		}
	}

}
