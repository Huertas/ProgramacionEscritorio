package Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.BLL.BLL_user;

import java.util.Calendar;

import Framework_Sergio_Huertas_Gisbert.Utils.*;
import Framework_Sergio_Huertas_Gisbert.Classes.Fecha;
import Framework_Sergio_Huertas_Gisbert.Classes.Language;
import Framework_Sergio_Huertas_Gisbert.Classes.Settings;
import Framework_Sergio_Huertas_Gisbert.Utils.F;

public class Fuser {
	public static String askDNI() {
		String DNI = "";
		boolean result = false;
		do {
			DNI = F.askString(Language.getInstance().getAskDNI(), "DNI");
			result = Validate.ValidateDNI(DNI);
		} while (result == false);
		return DNI;
	}

	public static String askName() {
		String name = "";
		boolean result = false;
		do {
			name = F.askString(Language.getInstance().getAskName(), Language.getInstance().getName());
			result = Validate.validateName(name);
		} while (result == false);
		return name;
	}

	public static String askemail() {
		String email = "";
		boolean result = false;
		do {
			email = F.askString(Language.getInstance().getAskEmail(), "Email");
			result = Validate.validateEmail(email);
		} while (result == false);
		return email;
	}

	public static int askSex() {
		String[] tipo = { Language.getInstance().getHombre(), Language.getInstance().getMujer() };
		int sex = 0;
		do {
			sex = F.menubuttons(tipo, Language.getInstance().getAskSex(), Language.getInstance().getSex());
		} while (sex == -1);
		return sex;
	}

	public static String askUserName() {
		String username = "";
		boolean result = false;
		do {
			username = F.askString(Language.getInstance().getAskUsername(), Language.getInstance().getUsername());
			result = Validate.validateName(username);
		} while (result == false);
		return username;
	}

	public static String askPass() {
		String pass = "";
		pass = F.askString(Language.getInstance().getAskPass(), Language.getInstance().getPass());
		return pass;
	}

	public static Fecha askBornDate() {
		String a = "";
		boolean c = false;
		Fecha b = null;
		if (Settings.getInstance().getDate_config() == 0) {
			do {
				try {
					a = F.askString(Language.getInstance().getAskBornDay() + " (dd/mm/yyyy)",
							Language.getInstance().getFecha());
					b = new Fecha(a);
					b=F.setFecha(b);
				} catch (Exception E) {
					continue;
				}
				c = b.validateDate();
			} while (c == false);
		} else if (Settings.getInstance().getDate_config() == 1) {
			do {
				try {
					a = F.askString(Language.getInstance().getAskBornDay() + " (dd-mm-yyyy)",
							Language.getInstance().getFecha());
					b = new Fecha(a);
					b=F.setFecha(b);
				} catch (Exception E) {
					continue;
				}
				c = b.validateDate();
			} while (c == false);
		} else if (Settings.getInstance().getDate_config() == 2) {
			do {
				try {
					a = F.askString(Language.getInstance().getAskBornDay() + " (yyyy/mm/dd)",
							Language.getInstance().getFecha());
					b = new Fecha(a);
					b=F.setFecha(b);
				} catch (Exception E) {
					continue;
				}
				c = b.validateDate();
			} while (c == false);
		} else if (Settings.getInstance().getDate_config() == 3) {
			do {
				try {
					a = F.askString(Language.getInstance().getAskBornDay() + " (yyyy-mm-dd)",
							Language.getInstance().getFecha());
					b = new Fecha(a);
					b=F.setFecha(b);
				} catch (Exception E) {
					continue;
				}
				c = b.validateDate();
			} while (c == false);
		}
		b = F.setFecha(b);
		return b;
	}

	public static Fecha askHireDate(Fecha bornDate) {
		String a = "";
		boolean c = false;
		Fecha b = null;
		if (Settings.getInstance().getDate_config() == 0) {
			do {
				try {
					a = F.askString(Language.getInstance().getAskHireDay() + " (dd/mm/yyyy)",
							Language.getInstance().getFecha());
					b = new Fecha(a);
					b=F.setFecha(b);
				} catch (Exception E) {
					continue;
				}
				c = b.validateDate();
				if(b.substract2Dates(bornDate)<=-16){
					c=true;
				}else{
					c=false;
				}
			} while (c == false);
		} else if (Settings.getInstance().getDate_config() == 1) {
			do {
				try {
					a = F.askString(Language.getInstance().getAskHireDay() + " (dd-mm-yyyy)",
							Language.getInstance().getFecha());
					b = new Fecha(a);
					b=F.setFecha(b);
				} catch (Exception E) {
					continue;
				}
				c = b.validateDate();
				if(b.substract2Dates(bornDate)<=-16){
					c=true;
				}else{
					c=false;
				}
			} while (c == false);
		} else if (Settings.getInstance().getDate_config() == 2) {
			do {
				try {
					a = F.askString(Language.getInstance().getAskHireDay() + " (yyyy/mm/dd)",
							Language.getInstance().getFecha());
					b = new Fecha(a);
					b=F.setFecha(b);
				} catch (Exception E) {
					continue;
				}
				c = b.validateDate();
				if(b.substract2Dates(bornDate)<=-16){
					c=true;
				}else{
					c=false;
				}
			} while (c == false);
		} else if (Settings.getInstance().getDate_config() == 3) {
			do {
				try {
					a = F.askString(Language.getInstance().getAskHireDay() + " (yyyy-mm-dd)",
							Language.getInstance().getFecha());
					b = new Fecha(a);
					b=F.setFecha(b);
				} catch (Exception E) {
					continue;
				}
				c = b.validateDate();
				if(b.substract2Dates(bornDate)<=-16){
					c=true;
				}else{
					c=false;
				}
			} while (c == false);
		}
		b = F.setFecha(b);
		return b;
	}

	public static int age(Fecha BornDate) {
		int age = 0;
		do {
			int year1;
			int year2;
			Calendar date1 = Calendar.getInstance();
			Calendar date2 = BornDate.string2Calen();

			year1 = date1.get(Calendar.YEAR);
			year2 = date2.get(Calendar.YEAR);
			age = year1 - year2;
		} while (age < 18);

		return (age);
	}

	public static int antiquity(Fecha hire) {
		int year1;
		int year2;
		Calendar date1 = Calendar.getInstance();
		Calendar date2 = hire.string2Calen();

		year1 = date1.get(Calendar.YEAR);
		year2 = date2.get(Calendar.YEAR);

		return year1 - year2;
	}

	public static Fecha GetDfirst() {
		Calendar date = Calendar.getInstance();
		String a = date.get(Calendar.YEAR) + "/" + date.get(Calendar.MONTH) + "/" + date.get(Calendar.DAY_OF_MONTH);
		Fecha dfirst = new Fecha(a);
		return dfirst;
	}

	public static int askPhone() {
		boolean a = false;
		int phone = 0;
		do {
			phone = F.askinteger(Language.getInstance().getAskPhone(), Language.getInstance().getPhone());
			a = Validate.Validatetlf(Integer.toString(phone));
		} while (a == false);
		return phone;
	}

	public static float askSalary() {
		boolean a = false;
		float salary = 0;
		do {
			try {
				salary = Float.parseFloat(
						F.askString(Language.getInstance().getAskSalary(), Language.getInstance().getSalary()));
				F.setCoin(salary);
			} catch (Exception e) {
				continue;
			}
			a = true;
		} while (a == false);
		return salary;
	}
}
