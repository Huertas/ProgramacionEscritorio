package Framework_Sergio_Huertas_Gisbert.Modules.Users.Utils;


import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.*;
import Framework_Sergio_Huertas_Gisbert.Utils.F;

public class FindUser {

	/* Find primary key
	 * @return location
	 */
	public static int find_adminDNI(Admin admin) { 
		for (int i = 0; i<=(Singleton.useradmin.size()-1); i++){
			if((Singleton.useradmin.get(i)).equals(admin) )
				return i;
		}
		return -1;
	}
	
	public static int find_clientDNI(Client client) { 
		for (int i = 0; i<=(Singleton.userclient.size()-1); i++){
			if((Singleton.userclient.get(i)).equals(client) )
				return i;
		}
		return -1;
	}
	
	public static int find_normalDNI(Normal user) { 
		for (int i = 0; i<=(Singleton.usernormal.size()-1); i++){
			if((Singleton.usernormal.get(i)).equals(user) )
				return i;
		}
		return -1;
	}
	public static String[] generate_vector_admin () {
		Admin a1 = null;
		String s = "";
		int arraylist =Singleton.useradmin.size();
		String [] user = new String[arraylist];
		for (int i = 0; i<arraylist; i++) {
			a1 = (Admin) Singleton.useradmin.get(i);
			s=a1.getDNI()+"-----"+a1.getName()+" "+a1.getUsername();
			user[i] = s;
		}
		
		return user;
	}
	
	public static String[] generate_vector_client () {
		Client c1 = null;
		String s = "";
		int arraylist =Singleton.userclient.size();
		String [] user = new String[arraylist];
		for (int i = 0; i<arraylist; i++) {
			c1 = (Client) Singleton.userclient.get(i);
			s=c1.getDNI()+"-----"+c1.getName()+" "+c1.getUsername();
			user[i] = s;
		}
		
		return user;
	}
	
	public static String[] generate_vector_Normal () {
		Normal u1 = null;
		String s = "";
		int arraylist =Singleton.usernormal.size();
		String [] user = new String[arraylist];
		for (int i = 0; i<arraylist; i++) {
			u1 = (Normal) Singleton.usernormal.get(i);
			s=u1.getDNI()+"-----"+u1.getName()+" "+u1.getUsername();
			user[i] = s;
		}
		
		return user;
	}
	
	public static Admin IDadmin () {
		Admin a1 = null;
		String ID = "";
		String [] admin = generate_vector_admin ();
		String search = F.combobox("select_user", "asktitle", admin);
		if (search != ""){
			for (int i = 0; i<9; i++) {
				ID += search.charAt(i);
			}
			a1 = new Admin (ID);
		}
		return a1;		
	}
	
	public static Client IDclient () {
		Client c1 = null;
		String ID = "";
		String [] client = generate_vector_client ();
		String search = F.combobox("select_user", "asktitle", client);
		System.out.println(search);
		if (search != ""){
			for (int i = 0; i<9; i++) {
				ID += search.charAt(i);
			}
			c1 = new Client (ID);
		}
		return c1;		
	}
	
	public static Normal IDNormal () {
		Normal u1 = null;
		String ID = "";
		String [] normal = generate_vector_Normal ();
		String search = F.combobox("select_user", "asktitle", normal);
		if (search != ""){
			for (int i = 0; i<9; i++) {
				ID += search.charAt(i);
			}
			u1 = new Normal (ID);
		}
		return u1;		
	}
}
