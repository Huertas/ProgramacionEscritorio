package Framework_Sergio_Huertas_Gisbert.Modules.Users.Utils;

import java.util.Collections;

import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.Order.*;
import Framework_Sergio_Huertas_Gisbert.Utils.F;
import Framework_Sergio_Huertas_Gisbert.Classes.Language;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.*;

public class Forder {

	/*
	 * Order arraylist
	 * 
	 * @return new order
	 */
	public static void change_orderadmin() {
		String[] options = { "DNI", Language.getInstance().getName(), Language.getInstance().getUsername(),
				Language.getInstance().getBornDay(), Language.getInstance().getAge() };
		int menu = -1;

		if (Singleton.useradmin.isEmpty()) {
			F.print("Error", "Error");
		} else {
			while (menu == -1) {
				menu = F.menubuttons(options, Language.getInstance().getAskOrder(), Language.getInstance().getMenu());
			}
			switch (menu) {
			case 0:
				Collections.sort(Singleton.useradmin);
				break;
			case 1:
				Collections.sort(Singleton.useradmin, new OrderName());
				break;
			case 2:
				Collections.sort(Singleton.useradmin, new OrderUsername());
				break;
			case 3:
				Collections.sort(Singleton.useradmin, new OrderDateBorn());
				break;
			case 4:
				Collections.sort(Singleton.useradmin, new OrderAge());
			}
		}
	}

	public static void change_orderclient() {
		String[] options = { "DNI", Language.getInstance().getName(), Language.getInstance().getUsername(),
				Language.getInstance().getBornDay(), Language.getInstance().getAge() };
		int menu = -1;

		if (Singleton.userclient.isEmpty()) {
			F.print("Error", "Error");
		} else {
			while (menu == -1) {
				menu = F.menubuttons(options, Language.getInstance().getAskOrder(), Language.getInstance().getMenu());
			}
			switch (menu) {
			case 0:
				Collections.sort(Singleton.userclient);
				break;
			case 1:
				Collections.sort(Singleton.userclient, new OrderName());
				break;
			case 2:
				Collections.sort(Singleton.userclient, new OrderUsername());
				break;
			case 3:
				Collections.sort(Singleton.userclient, new OrderDateBorn());
				break;
			case 4:
				Collections.sort(Singleton.userclient, new OrderAge());
			}
		}
	}

	public static void change_orderuser() {
		String[] options = { "A) DNI", "B) " + Language.getInstance().getName(),
				"C) " + Language.getInstance().getUsername() };
		int menu = -1;

		if (Singleton.usernormal.isEmpty()) {
			F.print("Error", "Error");
		} else {
			while (menu == -1) {
				menu = F.menubuttons(options, Language.getInstance().getAskOrder(), Language.getInstance().getMenu());
			}
			switch (menu) {
			case 0:
				Collections.sort(Singleton.usernormal);
				break;
			case 1:
				Collections.sort(Singleton.usernormal, new OrderName());
				break;
			case 2:
				Collections.sort(Singleton.usernormal, new OrderUsername());
				break;

			}
		}
	}

}
