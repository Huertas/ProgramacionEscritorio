package Framework_Sergio_Huertas_Gisbert.Main;

import Framework_Sergio_Huertas_Gisbert.Classes.Language;
import Framework_Sergio_Huertas_Gisbert.Classes.Settings;
import Framework_Sergio_Huertas_Gisbert.Classes.Files_Settings.SaveSettings;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.BLL.BLL_user.*;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Files_User.Automatic;
import Framework_Sergio_Huertas_Gisbert.Utils.F;

public class MainExecute {
	public static void exe() {
		Automatic.open();
		int option = 0;
		boolean a = false;
		while (a == false) {
			String[] tipo = { Language.getInstance().getCreateUser(), Language.getInstance().getReadUser(),
					Language.getInstance().getUpdateUser(), Language.getInstance().getDeleteUser(),
					Language.getInstance().getOrder(), Language.getInstance().getSave(),
					Language.getInstance().getOpen(), Language.getInstance().getSettings(),
					Language.getInstance().getExit() };
			option = F.menubuttons(tipo, Language.getInstance().getOptionMenu(), Language.getInstance().getMenu());
			if (option == -1 || option == 8) {
				SaveSettings.save();
				Automatic.save();
				System.exit(0);
			} else if (option == 0) {
				Crud.create();
			} else if (option == 1) {
				Crud.read();
			} else if (option == 2) {
				Crud.update();
			} else if (option == 3) {
				Crud.delete();
			} else if (option == 4) {
				Crud.order();
			} else if (option == 5) {
				Crud.save();
			} else if (option == 6) {
				Crud.open();
			} else {
				Settings.getInstance().SettingsMain();
			}
		}
	}

}
