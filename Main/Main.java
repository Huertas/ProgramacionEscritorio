package Framework_Sergio_Huertas_Gisbert.Main;

import Framework_Sergio_Huertas_Gisbert.Classes.Language;
import Framework_Sergio_Huertas_Gisbert.Classes.Files_Settings.OpenSettings;
import Framework_Sergio_Huertas_Gisbert.Utils.F;

public class Main {
	public static void exe() {
		OpenSettings.open();
		int option = -1;
		String[] tipo = { "Dummies", Language.getInstance().getUsers() };
		while (option == -1) {
			option = F.menubuttons(tipo, Language.getInstance().getAskDummies(), Language.getInstance().getQuestion());
			if (option == 1) {
				MainExecute.exe();
			} else if (option == 0) {
				Main_Dummies.exe();
			}
		}
	}
}
