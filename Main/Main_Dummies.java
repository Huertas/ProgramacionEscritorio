package Framework_Sergio_Huertas_Gisbert.Main;

import Framework_Sergio_Huertas_Gisbert.Classes.Language;
import Framework_Sergio_Huertas_Gisbert.Classes.Settings;
import Framework_Sergio_Huertas_Gisbert.Classes.Files_Settings.SaveSettings;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.DAO.*;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.BLL.BLL_user.*;
import Framework_Sergio_Huertas_Gisbert.Utils.F;

public class Main_Dummies {

	public static void exe() {
		String[] tipo1 = { Language.getInstance().getAdmin(), Language.getInstance().getClient(), "Normal" };
		int option1 = -1;
		while (option1 == -1) {
			option1 = F.menubuttons(tipo1, Language.getInstance().getAutofillQuestion(),
					Language.getInstance().getAutofill());
			if (option1 == 0) {
				Fuser_Dummies.makedummies_admin();
			} else if (option1 == 1) {
				Fuser_Dummies.makedummies_client();
			} else if (option1 == 2) {
				Fuser_Dummies.makedummies_normal();
			}
		}
		int option = 0;
		boolean a = false;
		while (a == false) {
			String[] tipo = { Language.getInstance().getCreateUser(), Language.getInstance().getReadUser(),
					Language.getInstance().getUpdateUser(), Language.getInstance().getDeleteUser(),
					Language.getInstance().getOrder(), Language.getInstance().getSave(),
					Language.getInstance().getOpen(), Language.getInstance().getSettings(),
					Language.getInstance().getExit() };
			option = F.menubuttons(tipo, Language.getInstance().getOptionMenu(), Language.getInstance().getMenu());
			if (option == -1 || option == 8) {
				SaveSettings.save();
				System.exit(0);
			} else if (option == 0) {
				Crud.create();
			} else if (option == 1) {
				Crud.read();
			} else if (option == 2) {
				Crud.update();
			} else if (option == 3) {
				Crud.delete();
			} else if (option == 4) {
				Crud.order();
			} else if (option == 5) {
				Crud.save();
			} else if (option == 6) {
				Crud.open();
			} else {
				Settings.getInstance().SettingsMain();
			}
		}

	}

}
