La aplicaci�n consta de una sencilla interfaz gr�fica (que puedes modificar dentro 
de los l�mites que permiten los ajustes) y funciona de la siguiente forma:
-Tienes la opci�n (nada m�s ejecutas) de utilizar usuarios de prueba (dummies) o
usuarios; estos primeros se crear�n 5 autom�ticamente (aleatorios) de un solo tipo,
puedes crear nuevos usuarios, borrarlos, ordenarlos, tienes un men� con diversas opciones
igualito al de los usuarios, solo que en el de los usuarios se guardar� autom�ticamente 
lo que hagas y no tendr�s que abrirlo cada vez.
- Una vez tengas las opciones a tu disposici�n tambi�n puedes modificar tu entorno desde
"ajustes", desde algo tan trivial como el formato de la fecha a una amplia gama 
de distintos idiomas! (actualmente operativos, 3)
- Notas a tener en cuenta:
	- Si quieres crear un usuario, la aplicaci�n no te dejar� no crearlo.
	- Para salir de la aplicaci�n tienes que ir al men� principal y darle a la "x"
o a "Salir"
	- Los clientes pueden tener cualquier rango de edad (no negativo)
	- Los admins solo pueden ser mayores o iguales a 16 a�os
	- Los usuarios no se les pregunta la fecha ya que sus opciones son bastante 
limitadas y si se interesa por la aplicaci�n no es la mejor manera pedirle 
una cantidad de datos que pueda considerar "exagerada".

- Mejoras: 

	Realmente, no s� qu� mejoras he hecho, la aplicaci�n hasta donde he podido 
comprobar funciona correctamente, he ido haciendo, creando c�digo, adapt�ndolo que
no me he fijado en si algo era una mejora o no

- Informaci�n sobre el c�digo:

	Tengo las funciones hechas sobre el karma, n�mero de comentarios y karmar 
pero realmente las veo muy chorras as� que no las he aplicado, ahora que estamos 
en android espero poder hacer algo chulo con esto.

	Todos los formatos se guardan siempre igual, la fecha por ejemplo con dd/mm/yyyy
y en los get es cuando lo modifico. Tambi�n puedes introducir en el formato que est�n las
cosas, si el formato es dd-mm-yyyy no tienes que ponerlo dd/mm/yyyy el c�digo se encarga
de transformarlo.


	Te adjunto una serie de errores que he ido corrigiendo durante estos �ltimos d�as:
	- Primer d�a DONE
	- Save-open auto DONE
	- Guardar Settings DONE
	- Theme en Settings DONE
	- Files Settings DONE
	- Hacer Crud.save y Crud.open DONE
	- Hacer men� de theme en Settings DONE
	- Arreglar la estructura del proyecto DONE
	- Men� combobox para ver qu� usuario quiero leer/eliminar DONE
	- Arreglar men� combobox para que no pete si le das a cancelar DONE
	- Comprobar que funciona el order    DONE
	- Hacer algo con el karma y el salario DONE
	- Corregir la fecha de contrataci�n DONE
	- Readme DONE
	- Traducir DONE
	- Formato fecha arreglar   Ver si funciona al introducir fecha en otro formato DONE
	- Buscar DNI al modificar el dni con el admin para que si lo quiere modificar
a uno que existe no le deje DONE
	- Mirar lo de los dummies el karma, karmar, num coment, salario... DONE
	- Mirar a la hora de introducir fechas, asegurarme de que no peta DONE
	- Arreglar el introducir el salario si est� en libras o d�lares DONE
	- Hacer lo mismo que con el read para el update y delete DONE
	










