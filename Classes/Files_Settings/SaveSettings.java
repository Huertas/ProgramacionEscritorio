package Framework_Sergio_Huertas_Gisbert.Classes.Files_Settings;

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;

import Framework_Sergio_Huertas_Gisbert.Classes.Settings;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.Usuario;

public class SaveSettings {
	public static void save() {
        String PATH = null;
	     
        try {
            PATH = new java.io.File(".").getCanonicalPath()
                    + "/src/Framework_Sergio_Huertas_Gisbert/Classes/Files_Settings/Files/Settings.json";
        } catch (IOException e) {
            e.printStackTrace();
        }
        
	    try {
		    XStream xstreamjson = new XStream(new JettisonMappedXmlDriver());
		    xstreamjson.setMode(XStream.NO_REFERENCES);
		    xstreamjson.alias("Settings", Settings.class);
		    Gson gson = new Gson();
		    String json = gson.toJson(Settings.getInstance());
		    FileWriter fileXml = new FileWriter(PATH);
		    fileXml.write(json.toString());
		    fileXml.close();
		    
		    
	    } catch (Exception e) {
	     	JOptionPane.showMessageDialog(null, "", "", JOptionPane.ERROR_MESSAGE);
	    }
	}
}
