package Framework_Sergio_Huertas_Gisbert.Classes.Files_Settings;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;

import Framework_Sergio_Huertas_Gisbert.Classes.Language;
import Framework_Sergio_Huertas_Gisbert.Classes.Settings;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.Admin;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.Singleton;

public class OpenSettings {

	public static void open() {
		String PATH = null;
		Settings config = null;

		try {
			XStream xstream = new XStream(new JettisonMappedXmlDriver());
			xstream.setMode(XStream.NO_REFERENCES);
			xstream.alias("Settings", Settings.class);

			PATH = new java.io.File(".").getCanonicalPath()
					+ "/src/Framework_Sergio_Huertas_Gisbert/Classes/Files_Settings/Files/Settings.json";

			JsonReader reader = new JsonReader(new FileReader(PATH));
			JsonParser parser = new JsonParser();
			JsonElement root = parser.parse(reader);

			Gson json = new Gson();
			config = json.fromJson(root, Settings.class);
			Settings.getInstance().setCurrency_config(config.getCurrency_config());
			Settings.getInstance().setDate_config(config.getDate_config());
			Settings.getInstance().setDecimals_config(config.getDecimals_config());
			Settings.getInstance().setFile(config.getFile());
			Settings.getInstance().setLanguage_config(config.getLanguage_config());
			Settings.getInstance().setTheme(config.getTheme());
			if (Settings.getInstance().getLanguage_config().equals("English")) {
				Language.getInstance().setLang(0);
			} else if (Settings.getInstance().getLanguage_config().equals("Castellano")) {
				Language.getInstance().setLang(1);
			} else if (Settings.getInstance().getLanguage_config().equals("Valenci�")) {
				Language.getInstance().setLang(2);

			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "", "", JOptionPane.ERROR_MESSAGE);
		}

	}
}
