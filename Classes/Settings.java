package Framework_Sergio_Huertas_Gisbert.Classes;

import java.io.Serializable;
import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.Admin;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.Client;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.Normal;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.Classes.Singleton;
import Framework_Sergio_Huertas_Gisbert.Utils.F;
import Framework_Sergio_Huertas_Gisbert.Utils.FThemes;

@XStreamAlias("Settings")
public class Settings implements Serializable {
	private static Settings instance;

	public static Settings getInstance() {
		if (instance == null) {
			instance = new Settings();
			FThemes.theme();
			Singleton.useradmin = new ArrayList<Admin>();
			Singleton.userclient = new ArrayList<Client>();
			Singleton.usernormal = new ArrayList<Normal>();
		}
		return instance;
	}

	@XStreamAlias("language_config")
	private String language_config = "English";
	@XStreamAlias("currency_config")
	private String currency_config = "�";
	@XStreamAlias("date_config")
	private int date_config = 0;
	@XStreamAlias("decimals_config")
	private int decimals_config = 2;
	@XStreamAlias("theme")
	private String theme = "METAL";
	@XStreamAlias("file")
	private int file = 0; // json

	/*
	 * private Settings(int date_config, String language_config, char
	 * currency_config, int decimals_config) { super(); this.date_config =
	 * date_config;// dd/mm/yyyy dd-mm-yyyy this.language_config =
	 * language_config; this.currency_config = currency_config;
	 * this.decimals_config = decimals_config; }
	 */
	private Settings() {
		super();
		this.date_config = 0; // dd/mm/yyyy
		this.language_config = "English";
		this.currency_config = "�";
		this.decimals_config = 2;
		this.theme = "METAL";
		this.file = 0;
	}

	public int getFile() {
		return file;
	}

	public void setFile(int file) {
		this.file = file;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
		FThemes.theme();
	}

	public int getDate_config() {
		return date_config;
	}

	public void setDate_config(int date_config) {
		this.date_config = date_config;
	}

	public String getLanguage_config() {
		return language_config;
	}

	public void setLanguage_config(String language_config) {
		this.language_config = language_config;
	}

	public String getCurrency_config() {
		return currency_config;
	}

	public void setCurrency_config(String currency_config) {
		this.currency_config = currency_config;
	}

	public int getDecimals_config() {
		return decimals_config;
	}

	public void setDecimals_config(int decimals_config) {
		this.decimals_config = decimals_config;
	}

	public void SettingsMain() {

		String[] fecha = { "dd/mm/yyyy", "dd-mm-yyyy", "yyyy/mm/dd", "yyyy-mm-dd" };
		String[] lang = { "English", "Castellano", "Valenci�" };
		String[] moneda = { "�", "$", "�" };
		String[] dec = { ".#", ".##", ".###" };
		String[] file = { "JSON", "TXT" };
		String[] theme = { "METAL", "GTK", "MOTIF", "NINBUS", "WINDOWS95", "WINDOWS" };
		int option = 0;
		int fechaconf = 0;
		int langconf = 0;
		int monedaconf = 0;
		int decconf = 0;
		int fileconf = 0;
		int themeconf = 0;
		while (option != 6) {
			String[] tipo = { Language.getInstance().getDateFormat(), Language.getInstance().getLanguageFormat(),
					Language.getInstance().getDecimalFormat(), Language.getInstance().getCurrencyFormat(),
					Language.getInstance().getFile(), Language.getInstance().getTheme(),
					Language.getInstance().getExit() };
			option = F.menubuttons(tipo, Language.getInstance().getOptionMenu(), Language.getInstance().getMenu());
			if (option == 0) {
				fechaconf = F.menubuttons(fecha, Language.getInstance().getAskDateFormat(),
						Language.getInstance().getDateFormat());
				if (fechaconf == 0) {
					Settings.getInstance().setDate_config(fechaconf);
				} else if (fechaconf == 1) {
					Settings.getInstance().setDate_config(fechaconf);
				} else if (fechaconf == 2) {
					Settings.getInstance().setDate_config(fechaconf);
				} else if (fechaconf == 3) {
					Settings.getInstance().setDate_config(fechaconf);
				}
			} else if (option == 1) {
				langconf = F.menubuttons(lang, Language.getInstance().getAskLanguageFormat(),
						Language.getInstance().getLanguageFormat());
				if (langconf == 0) {
					Settings.getInstance().setLanguage_config("English");
					Language.getInstance().setLang(0);
				} else if (langconf == 1) {
					Settings.getInstance().setLanguage_config("Castellano");
					Language.getInstance().setLang(1);
				} else if (langconf == 2) {
					Settings.getInstance().setLanguage_config("Valenci�");
					Language.getInstance().setLang(2);
				}
			} else if (option == 2) {
				decconf = F.menubuttons(dec, Language.getInstance().getAskDecimalFormat(),
						Language.getInstance().getDecimalFormat());
				if (decconf == 0) {
					Settings.getInstance().setDecimals_config(1);
				} else if (decconf == 1) {
					Settings.getInstance().setDecimals_config(2);
				} else if (decconf == 2) {
					Settings.getInstance().setDecimals_config(3);
				}
			} else if (option == 3) {
				monedaconf = F.menubuttons(moneda, Language.getInstance().getAskCurrencyFormat(),
						Language.getInstance().getCurrencyFormat());
				if (monedaconf == 0) {
					Settings.getInstance().setCurrency_config(moneda[0]);
				}
				if (monedaconf == 1) {
					Settings.getInstance().setCurrency_config(moneda[1]);
				}
				if (monedaconf == 2) {
					Settings.getInstance().setCurrency_config(moneda[2]);
				}
			} else if (option == 4) {
				fileconf = F.menubuttons(file, Language.getInstance().getAskFile(), Language.getInstance().getFile());
				if (fileconf == 0) {
					Settings.getInstance().setFile(0);
				} else if (fileconf == 1) {
					Settings.getInstance().setFile(1);
				}

			} else if (option == 5) {
				themeconf = F.menubuttons(theme, Language.getInstance().getAskTheme(), Language.getInstance().getTheme());
				switch (themeconf) {
				case 0:// Metal - Predeterminado JAVA
					Settings.getInstance().setTheme("METAL");
					// Functions_theme.theme();
					break;

				case 1:// GTK
					Settings.getInstance().setTheme("GTK");
					// Functions_theme.theme();
					break;

				case 2:// Motif
					Settings.getInstance().setTheme("MOTIF");
					// Functions_theme.theme();
					break;

				case 3:// Nimbus - JAVA
					Settings.getInstance().setTheme("NINBUS");
					// Functions_theme.theme();
					break;

				case 4:// WINDOWS 95
					Settings.getInstance().setTheme("WINDOWS95");
					// Functions_theme.theme();
					break;

				case 5:// WINDOWS
					Settings.getInstance().setTheme("WINDOWS");
					// Functions_theme.theme();
					break;

				}

			}
		}
	}

	@Override
	public String toString() {
		return "Config [date_config=" + date_config + ", language_config=" + language_config + ", currency_config="
				+ currency_config + ", decimals_config=" + decimals_config + "]";
	}
}
