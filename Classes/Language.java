package Framework_Sergio_Huertas_Gisbert.Classes;

public class Language {
	private static Language instance;

	// Language.getInstance().getError();
	private int lang;

	public static Language getInstance() {
		if (instance == null)
			instance = new Language(0);
		return instance;
	}

	private Language(int lang) {
		this.lang = lang;
	}

	public int getLang() {
		return lang;
	}

	public void setLang(int lang) {
		this.lang = lang;
	}

	public String getErrorCharacter() {
		switch (lang) {
		case (1):
			return "No has introducido un car�cter";
		case (2):
			return "No has introdu�t un car�cter";
		default:
			return "You haven't introduced a character";
		}
	}

	public String getCreateUser() {
		switch (lang) {
		case (1):
			return "Crear usuario";
		case (2):
			return "Crear usuari";
		default:
			return "Create user";
		}
	}

	public String getReadUser() {
		switch (lang) {
		case (1):
			return "Leer usuario";
		case (2):
			return "Llegir usuari";
		default:
			return "Read user";
		}
	}

	public String getUpdateUser() {
		switch (lang) {
		case (1):
			return "Actualizar usuario";
		case (2):
			return "Actualitzar usuari";
		default:
			return "Update user";
		}
	}

	public String getDeleteUser() {
		switch (lang) {
		case (1):
			return "Borrar usuario";
		case (2):
			return "Borrar usuari";
		default:
			return "Delete user";
		}
	}

	public String getSettings() {
		switch (lang) {
		case (1):
			return "Ajustes";
		case (2):
			return "Ajustos";
		default:
			return "Settings";
		}
	}

	public String getExit() {
		switch (lang) {
		case (1):
			return "Salir";
		case (2):
			return "Eixir";
		default:
			return "Exit";
		}
	}

	public String getMenu() {
		switch (lang) {
		case (1):
			return "Men�";
		case (2):
			return "Men�";
		default:
			return "Menu";
		}
	}

	public String getOptionMenu() {
		switch (lang) {
		case (1):
			return "Elige la opci�n del men�";
		case (2):
			return "Eligeix la opci� del men�";
		default:
			return "Choose the option of the menu";
		}
	}

	public String getUser() {
		switch (lang) {
		case (1):
			return "Usuario";
		case (2):
			return "Usuari";
		default:
			return "User";
		}
	}public String getUsers() {
		switch (lang) {
		case (1):
			return "Usuarios";
		case (2):
			return "Usuaris";
		default:
			return "Users";
		}
	}

	public String getAdmin() {
		switch (lang) {
		case (1):
			return "Administrador";
		case (2):
			return "Administrador";
		default:
			return "Admin";
		}
	}

	public String getClient() {
		switch (lang) {
		case (1):
			return "Cliente";
		case (2):
			return "Client";
		default:
			return "Client";
		}
	}

	public String getAskUpdate() {
		switch (lang) {
		case (1):
			return "Qu� quieres actualizar?";
		case (2):
			return "Qu� vols actualitzar?";
		default:
			return "What do you want to update?";
		}
	}

	public String getName() {
		switch (lang) {
		case (1):
			return "Nombre";
		case (2):
			return "Nom";
		default:
			return "Name";
		}
	}

	public String getUsername() {
		switch (lang) {
		case (1):
			return "Nombre de usuario";
		case (2):
			return "Nom d'usuari";
		default:
			return "Username";
		}
	}

	public String getPass() {
		switch (lang) {
		case (1):
			return "Contrase�a";
		case (2):
			return "Contrasenya";
		default:
			return "Password";
		}
	}

	public String getBornDay() {
		switch (lang) {
		case (1):
			return "D�a de nacimiento";
		case (2):
			return "Dia de naiximent";
		default:
			return "Born day";
		}
	}

	public String getHireDay() {
		switch (lang) {
		case (1):
			return "D�a de contrataci�n";
		case (2):
			return "Dia de contrataci�";
		default:
			return "Hire day";
		}
	}

	public String getPhone() {
		switch (lang) {
		case (1):
			return "Tel�fono";
		case (2):
			return "Tel�fon";
		default:
			return "Phone";
		}
	}

	public String getSalary() {
		switch (lang) {
		case (1):
			return "Salario";
		case (2):
			return "Salari";
		default:
			return "Salary";
		}
	}

	public String getCommentNumber() {
		switch (lang) {
		case (1):
			return "N�mero de comentarios";
		case (2):
			return "Nombre de comentaris";
		default:
			return "Comment number";
		}
	}

	public String getNoNumber() {
		switch (lang) {
		case (1):
			return "No has introducido un n�mero";
		case (2):
			return "No has introdu�t un nombre";
		default:
			return "You haven't introduced a number";
		}
	}

	public String getDolar() {
		switch (lang) {
		case (1):
			return "D�lar";
		case (2):
			return "D�lar";
		default:
			return "Dollar";
		}
	}

	public String getLibra() {
		switch (lang) {
		case (1):
			return "Libra";
		case (2):
			return "Llibra";
		default:
			return "Pound";
		}
	}

	public String getCurrency() {
		switch (lang) {
		case (1):
			return "Moneda";
		case (2):
			return "Moneda";
		default:
			return "Currency";
		}
	}

	public String getDecimals() {
		switch (lang) {
		case (1):
			return "Decimales";
		case (2):
			return "Decimals";
		default:
			return "Decimals";
		}
	}

	public String getFecha() {
		switch (lang) {
		case (1):
			return "Fecha";
		case (2):
			return "Data";
		default:
			return "Date";
		}
	}

	public String getAskDNI() {
		switch (lang) {
		case (1):
			return "Introduce tu DNI";
		case (2):
			return "Introdueix el teu DNI";
		default:
			return "Introduce your DNI";
		}
	}

	public String getAskName() {
		switch (lang) {
		case (1):
			return "Introduce tu nombre";
		case (2):
			return "Introdueix el teu nom";
		default:
			return "Introduce your name";
		}
	}

	public String getAskEmail() {
		switch (lang) {
		case (1):
			return "Introduce tu email";
		case (2):
			return "Introdueix el teu email";
		default:
			return "Introduce your email";
		}
	}

	public String getHombre() {
		switch (lang) {
		case (1):
			return "Hombre";
		case (2):
			return "Home";
		default:
			return "Man";
		}
	}

	public String getMujer() {
		switch (lang) {
		case (1):
			return "Mujer";
		case (2):
			return "Dona";
		default:
			return "Woman";
		}
	}

	public String getSex() {
		switch (lang) {
		case (1):
			return "Sexo";
		case (2):
			return "Sexe";
		default:
			return "Sex";
		}
	}

	public String getAskSex() {
		switch (lang) {
		case (1):
			return "Elige tu sexo";
		case (2):
			return "Elegeix el teu sexe";
		default:
			return "Choose your sex";
		}
	}

	public String getAskUsername() {
		switch (lang) {
		case (1):
			return "Introduce tu nombre de usuario";
		case (2):
			return "Introdueix el teu nom d'usuari";
		default:
			return "Introduce your username";
		}
	}

	public String getAskPass() {
		switch (lang) {
		case (1):
			return "Introduce tu contrase�a";
		case (2):
			return "Introdueix la teua contrasenya";
		default:
			return "Introduce your password";
		}
	}

	public String getAskBornDay() {
		switch (lang) {
		case (1):
			return "Introduce tu d�a de nacimiento en el formato";
		case (2):
			return "Introdueix el teu dia de naiximent en el format";
		default:
			return "Introduce the day you were born in the format";
		}
	}

	public String getAskHireDay() {
		switch (lang) {
		case (1):
			return "Introduce el d�a que fuiste contratado en el formato";
		case (2):
			return "Introdueix el dia en que et van contratar en el format";
		default:
			return "Introduce the day you were hired in the format";
		}
	}

	public String getAskSalary() {
		switch (lang) {
		case (1):
			return "Introduce tu salario";
		case (2):
			return "Introdueix el teu salari";
		default:
			return "Introduce your salary";
		}
	}

	public String getAskPhone() {
		switch (lang) {
		case (1):
			return "Introduce tu n�mero de tel�fono";
		case (2):
			return "Introdueix el teu tel�fon";
		default:
			return "Introduce your phone number";
		}
	}

	public String getDateFormat() {
		switch (lang) {
		case (1):
			return "Formato de fecha";
		case (2):
			return "Format de data";
		default:
			return "Date Format";
		}
	}

	public String getAskDateFormat() {
		switch (lang) {
		case (1):
			return "Qu� formato de fecha quieres?";
		case (2):
			return "Quin format de data vols?";
		default:
			return "Which date dormat do you want?";
		}
	}

	public String getLanguageFormat() {
		switch (lang) {
		case (1):
			return "Formato de lenguaje";
		case (2):
			return "Format de llenguatge";
		default:
			return "Language format";
		}
	}

	public String getAskLanguageFormat() {
		switch (lang) {
		case (1):
			return "Qu� idioma quieres?";
		case (2):
			return "Quin idioma vols?";
		default:
			return "Which language do you want?";
		}
	}

	public String getDecimalFormat() {
		switch (lang) {
		case (1):
			return "Formato de decimales";
		case (2):
			return "Format de decimals";
		default:
			return "Decimals format";
		}
	}

	public String getAskDecimalFormat() {
		switch (lang) {
		case (1):
			return "Qu� formato de decimales quieres?";
		case (2):
			return "Quin format de decimals vols?";
		default:
			return "Which decimal format do you want?";
		}
	}

	public String getCurrencyFormat() {
		switch (lang) {
		case (1):
			return "Formato de moneda";
		case (2):
			return "Format de moneda";
		default:
			return "Currency format";
		}
	}

	public String getAskCurrencyFormat() {
		switch (lang) {
		case (1):
			return "Qu� formato de moneda quieres?";
		case (2):
			return "Quin format de moneda vols?";
		default:
			return "Which currency format do you want?";
		}
	}

	public String getAge() {
		switch (lang) {
		case (1):
			return "Edad";
		case (2):
			return "Edad";
		default:
			return "Age";
		}
	}

	public String getAntiquity() {
		switch (lang) {
		case (1):
			return "Antig�edad";
		case (2):
			return "Antiguetat";
		default:
			return "Antiquity";
		}
	}

	public String getFirstDay() {
		switch (lang) {
		case (1):
			return "Primer d�a";
		case (2):
			return "Primer dia";
		default:
			return "First day";
		}
	}

	public String getChainError() {
		switch (lang) {
		case (1):
			return "No has introducido una cadena";
		case (2):
			return "No has introdu�t una cadena";
		default:
			return "You haven't introduced a chain";
		}
	}

	public String getAskUser() {
		switch (lang) {
		case (1):
			return "Qu� tipo de usuario quieres?";
		case (2):
			return "Quin tipus d'usuari vols?";
		default:
			return "Which type of user do you want?";
		}
	}public String getAutofill() {
		switch (lang) {
		case (1):
			return "Rellenar autom�ticamente";
		case (2):
			return "Rellenar autom�ticament";
		default:
			return "Autofill";
		}
	}public String getAutofillQuestion() {
		switch (lang) {
		case (1):
			return "Qu� quieres rellenar autom�ticamente";
		case (2):
			return "Qu� vols rellenar autom�ticament";
		default:
			return "What do you want to autofill?";
		}
	}public String getOrder() {
		switch (lang) {
		case (1):
			return "Ordenar";
		case (2):
			return "Ordenar";
		default:
			return "Order";
		}
	}public String getOpen() {
		switch (lang) {
		case (1):
			return "Abrir";
		case (2):
			return "Obrir";
		default:
			return "Open";
		}
	}public String getSave() {
		switch (lang) {
		case (1):
			return "Guardar";
		case (2):
			return "Guardar";
		default:
			return "Save";
		}
	}public String getQuestion() {
		switch (lang) {
		case (1):
			return "Pregunta";
		case (2):
			return "Pregunta";
		default:
			return "Question";
		}
	}public String getAskDummies() {
		switch (lang) {
		case (1):
			return "Quieres usar dummies?";
		case (2):
			return "Vols usar dummies?";
		default:
			return "Do you want to use dummies?";
		}
	}public String getUserAlreadyRegistered() {
		switch (lang) {
		case (1):
			return "El usuario est� ya registrado";
		case (2):
			return "El usuari est� ja registrar";
		default:
			return "The user is already registred";
		}
	}public String getTheme() {
		switch (lang) {
		case (1):
			return "Tema";
		case (2):
			return "Tema";
		default:
			return "Theme";
		}
	}public String getFile() {
		switch (lang) {
		case (1):
			return "Archivo";
		case (2):
			return "Arxiu";
		default:
			return "File";
		}
	}public String getAskFile() {
		switch (lang) {
		case (1):
			return "Elige el tipo de archivo";
		case (2):
			return "Eligeix el tipus d'arxiu";
		default:
			return "Choose the type of file";
		}
	}public String getAskTheme() {
		switch (lang) {
		case (1):
			return "Elige el tipo de tema";
		case (2):
			return "Eligeix el tipus de tema";
		default:
			return "Choose the type of theme";
		}
	}public String getAskOrder() {
		switch (lang) {
		case (1):
			return "Qu� orden quieres?";
		case (2):
			return "Quin ordre vols?";
		default:
			return "�What order do you prefer?";
		}
	}public String getAll() {
		switch (lang) {
		case (1):
			return "Todos";
		case (2):
			return "Tots";
		default:
			return "All";
		}
	}public String getJustOne() {
		switch (lang) {
		case (1):
			return "Solo uno";
		case (2):
			return "Sols un";
		default:
			return "Just one";
		}
	}public String getNoUsersRegistered() {
		switch (lang) {
		case (1):
			return "No hay usuarios registrados";
		case (2):
			return "No hi han usuaris registrats";
		default:
			return "No users registered";
		}
	}public String getChoose() {
		switch (lang) {
		case (1):
			return "Elige";
		case (2):
			return "Eligeix";
		default:
			return "Choose";
		}
	}public String getUserNotFound() {
		switch (lang) {
		case (1):
			return "Usuario no encontrado";
		case (2):
			return "No s'ha trobat l'usuari";
		default:
			return "User not found";
		}
	}public String getAskSave() {
		switch (lang) {
		case (1):
			return "Qu� quieres guardar";
		case (2):
			return "Qu� vols guardar";
		default:
			return "What do you want to save?";
		}
	}public String getAskOpen() {
		switch (lang) {
		case (1):
			return "Qu� quieres abrir";
		case (2):
			return "Qu� vols obrir";
		default:
			return "What do you want to open?";
		}
	}public String getHow() {
		switch (lang) {
		case (1):
			return "C�mo?";
		case (2):
			return "Com?";
		default:
			return "How?";
		}
	}public String getAskWhatToOrder() {
		switch (lang) {
		case (1):
			return "Qu� quieres ordenar";
		case (2):
			return "Qu� vols ordenar";
		default:
			return "What do you want to order?";
		}
	}public String getAskDelete() {
		switch (lang) {
		case (1):
			return "Qu� quieres eliminar";
		case (2):
			return "Qu� vols eliminar";
		default:
			return "What do you want to delete?";
		}
	}
}
