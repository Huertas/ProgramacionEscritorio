package Framework_Sergio_Huertas_Gisbert.Classes;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Fecha")
public class Fecha implements Serializable {
	@XStreamAlias("fecha")
	private String fecha;

	public Fecha(String fecha) {

		this.fecha = fecha;
	}

	public String getFecha() {
		Date date = null;
		SimpleDateFormat sdf;
		try {
			sdf = new SimpleDateFormat("dd/mm/yyyy");
			date = sdf.parse(fecha);
		} catch (Exception e) {
			return fecha;
		}
		if (Settings.getInstance().getDate_config() == 0)
			sdf = new SimpleDateFormat("dd/mm/yyyy");
		else if (Settings.getInstance().getDate_config() == 1)
			sdf = new SimpleDateFormat("dd-mm-yyyy");
		else if (Settings.getInstance().getDate_config() == 2)
			sdf = new SimpleDateFormat("yyyy/mm/dd");
		else if (Settings.getInstance().getDate_config() == 3)
			sdf = new SimpleDateFormat("yyyy-mm-dd");
		return sdf.format(date);
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	// metodo per a pasar de string a calendar
	public Calendar string2Calen() {

		Calendar fecha = Calendar.getInstance();
		Date date2 = null;

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
			date2 = sdf.parse(this.fecha);
			fecha.setTime(date2);

		} catch (Exception e) {
			return fecha;

		}

		return fecha;
	}

	// metodo per a pasar de calendar a string

	@Override
	public String toString() {
		return fecha;
	}

	// metodo per obtindre la fecha del sistema
	public Calendar fechaSistema() {
		Calendar fecha = Calendar.getInstance();
		return fecha;
	}

	// metodo per a validar una fecha
	public boolean validateDate() {

		int dia;
		int mes;
		int anyo;
		boolean validate = false;
		String[] fechaArray = null;
		try {
			fechaArray = this.fecha.split("/");

			dia = Integer.parseInt(fechaArray[0]);
			mes = Integer.parseInt(fechaArray[1]);
			anyo = Integer.parseInt(fechaArray[2]);
		} catch (Exception e) {
			return false;
		}

		if ((anyo > 1950) && (anyo < 2016)) {
			if ((mes >= 1) && (mes <= 12)) {
				switch (mes) {
				case 1: // Enero
					if ((dia > 0) && (dia <= 31))
						validate = true;
					break;
				case 2: // Febrero

					if ((((anyo % 100 == 0) && (anyo % 400 == 0)) || ((anyo % 100 != 0) && (anyo % 4 == 0)))
							&& (dia > 0) && (dia <= 29))
						validate = true; // A�o Bisiesto
					else if ((dia > 0) && (dia <= 28))
						validate = true;
					break;
				case 3: // Marzo
					if ((dia > 0) && (dia <= 31))
						validate = true;
					break;
				case 4: // Abril
					if ((dia > 0) && (dia <= 30))
						validate = true;
					break;
				case 5: // Mayo
					if ((dia > 0) && (dia <= 31))
						validate = true;
					break;
				case 6: // Junio
					if ((dia > 0) && (dia <= 30))
						validate = true;
					break;
				case 7: // Julio
					if ((dia > 0) && (dia <= 31))
						validate = true;
					break;
				case 8: // Agosto
					if ((dia > 0) && (dia <= 31))
						validate = true;
					break;
				case 9: // Septiembre
					if ((dia > 0) && (dia <= 30))
						validate = true;
					break;
				case 10: // Octubre
					if ((dia > 0) && (dia <= 31))
						validate = true;
					break;
				case 11: // Noviembre
					if ((dia > 0) && (dia <= 30))
						validate = true;
					break;
				case 12: // Diciembre

					if ((dia > 0) && (dia <= 31))
						validate = true;
					break;

				default:
					validate = false;
				}
			} else
				validate = false;
		} else
			validate = false;
		return validate;
	}

	// metodo per a saber si this.fecha1 es anterior a fecha sistema
	public int compareDateSystem() { // si fecha1 anterior retorna 0
		// si fecha1 igual retorna 1
		int var = 0; // si fecha1 posterior retorna 2
		Calendar date1 = this.string2Calen();
		Calendar date2 = Calendar.getInstance();

		if (date1.before(date2))
			var = 0;
		else if (date1.equals(date2))
			var = 1;
		else if (date1.after(date2))
			var = 2;

		return var;

	}

	// metodo per a saber si this.fecha1 es anterior a fecha2
	public int compareDate(Fecha date2) { // si fecha1 anterior retorna 0
		// si fecha1 igual retorna 1
		int var = 0; // si fecha1 posterior retorna 2
		Calendar date1 = this.string2Calen();
		Calendar dateCalen2;

		dateCalen2 = date2.string2Calen();

		if (date1.before(dateCalen2))
			var = -1;
		else if (date1.equals(dateCalen2))
			var = 0;
		else if (date1.after(dateCalen2))
			var = 1;
		return var;

	}

	// metodo per a restar els anys de dos feches. This.fecha menos fecha
	// arreplegada
	public int substract2Dates(Fecha date2) {

		int year1;
		int year2;
		Calendar date1 = this.string2Calen();
		Calendar date2Calen = date2.string2Calen();

		year1 = date1.get(Calendar.YEAR);
		year2 = date2Calen.get(Calendar.YEAR);

		return (year2 - year1);

	}

	// metodo per a restar els anys de dos feches. Fecha sistema menos
	// This.fecha
	public int substractSystemDate() {

		int year1;
		int year2;
		Calendar date1 = Calendar.getInstance();
		Calendar date2 = this.string2Calen();

		year1 = date1.get(Calendar.YEAR);
		year2 = date2.get(Calendar.YEAR);

		return (year1 - year2);

	}
}
