package Framework_Sergio_Huertas_Gisbert.Utils;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import Framework_Sergio_Huertas_Gisbert.Classes.Settings;



public class FThemes {
	public static void theme (){
		
		try {
			switch (Settings.getInstance().getTheme()){
			case "METAL":// Metal - Predeterminado JAVA
				UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
				break;

			case "GTK":// GTK
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				break;

			case "MOTIF":// Motif
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
				break;

			case "NINBUS":// Nimbus - JAVA
				//LookAndFeelInfo[] lafs = UIManager.getInstalledLookAndFeels();
				//for (LookAndFeelInfo laf: lafs){
					//if ("Nimbus".equals(laf.getName()))
						//UIManager.setLookAndFeel(laf.getClassName());
				//}
				UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
				break;	
				
			case "WINDOWS95":// WINDOWS 95
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel");
				break;
				
			case "WINDOWS":// WINDOWS
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
				break;
				
				
			}
		}catch (Exception e){
			JOptionPane.showMessageDialog(null, "Error theme", "Error title", JOptionPane.ERROR_MESSAGE);
		}
	}
}
