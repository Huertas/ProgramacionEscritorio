package Framework_Sergio_Huertas_Gisbert.Utils;

import javax.swing.JOptionPane;

import Framework_Sergio_Huertas_Gisbert.Classes.Fecha;
import Framework_Sergio_Huertas_Gisbert.Classes.Language;
import Framework_Sergio_Huertas_Gisbert.Classes.Settings;
import Framework_Sergio_Huertas_Gisbert.Modules.Users.Users.Model.BLL.BLL_user.*;

public class F {
	public static float Salary(int Karma, float Salary, Fecha dhire){
		Salary=Salary+20*Fuser.antiquity(dhire);
		Salary=Salary+(5*(Karma/100.0f));
		return Salary;
	}
	//Funciona pero no en uso
	public static int Karma(int numcomment){
		return numcomment/1000;
	}//Funciona pero no en uso
	public static String Karmar(int Karma){
		String karmar;
		if(Karma<200)
			karmar="low";
		else if(Karma<500)
			karmar="medium";
		else if(Karma<800)
			karmar="high";
		else if(Karma<990)
			karmar="really high";
		else if(Karma<=999)
			karmar="wow";
		else
			karmar="WOW";
		return karmar;
	}
	public static String combobox(String mensdesp, String titldesp, String[] opciones) {
		String op = "";

		// menu desplegable
		Object seleccion = JOptionPane.showInputDialog(null, mensdesp, titldesp, JOptionPane.QUESTION_MESSAGE, null,
				opciones, 0); // null para icono defecto

		if (seleccion == null) {
			op = "";
		} else {
			op = (String) seleccion;
		}
		return op;
	}

	public static float setCoin(float a) { // Para guardarlo internamente
		if (Settings.getInstance().getCurrency_config().equals("$")) {
			a = a / 1.05f;
		} else if (Settings.getInstance().getCurrency_config().equals("�")) {
			a = a * 0.85761f;
		}
		return a;
	}

	public static float getCoin(float a) { // Para imprimir al usuario
		if (Settings.getInstance().getCurrency_config().equals("$")) {
			a = a * 1.05f;
		} else if (Settings.getInstance().getCurrency_config().equals("�")) {
			a = a / 0.85761f;
		}
		return a;
	}

	public static float getDecimalFormat(float a) {
		if (Settings.getInstance().getDecimals_config() == 1)
			a = Formato.format1d(a);
		else if (Settings.getInstance().getDecimals_config() == 2)
			a = Formato.format2d(a);
		else if (Settings.getInstance().getDecimals_config() == 3)
			a = Formato.format3d(a);
		return a;
	}
	public static Fecha setFirstDay(Fecha dfirst){
		String[] fechaArray = null;
		String c = dfirst.toString();
		int dia, mes, anyo;
		fechaArray = null;
		c = dfirst.toString();
		fechaArray = c.split("/");
		dia = Integer.parseInt(fechaArray[2]);
		mes = Integer.parseInt(fechaArray[1])+1;
		anyo = Integer.parseInt(fechaArray[0]);
		dfirst.setFecha(dia + "/" + mes + "/" + anyo);
		return dfirst;
	}

	public static Fecha setFecha(Fecha a) {
		String[] fechaArray = null;
		String c = a.toString();
		int dia, mes, anyo;
		try{
		if (Settings.getInstance().getDate_config() == 1) {
			fechaArray = null;
			c = a.toString();
			fechaArray = c.split("-");
			dia = Integer.parseInt(fechaArray[0]);
			mes = Integer.parseInt(fechaArray[1]);
			anyo = Integer.parseInt(fechaArray[2]);
			a.setFecha(dia + "/" + mes + "/" + anyo);
		} else if (Settings.getInstance().getDate_config() == 2) {
			fechaArray = null;
			c = a.toString();
			fechaArray = c.split("/");
			dia = Integer.parseInt(fechaArray[2]);
			mes = Integer.parseInt(fechaArray[1]);
			anyo = Integer.parseInt(fechaArray[0]);
			a.setFecha(dia + "/" + mes + "/" + anyo);

		} else if (Settings.getInstance().getDate_config() == 3) {
			fechaArray = null;
			c = a.toString();
			fechaArray = c.split("-");
			dia = Integer.parseInt(fechaArray[2]);
			mes = Integer.parseInt(fechaArray[1]);
			anyo = Integer.parseInt(fechaArray[0]);
			a.setFecha(dia + "/" + mes + "/" + anyo);

		}
		return a;}
		catch(Exception e){
			Fecha b=new Fecha("1/1/1");
			return b;
		}
	}

	public static Fecha getFecha(Fecha a) {
		String[] fechaArray = null;
		String c = a + "";
		int dia, mes, anyo;
		Fecha b;
		if (Settings.getInstance().getDate_config() == 1) {
			fechaArray = c.split("/");
			dia = Integer.parseInt(fechaArray[0]);
			mes = Integer.parseInt(fechaArray[1]);
			anyo = Integer.parseInt(fechaArray[2]);
			b = new Fecha(dia + "-" + mes + "-" + anyo);
			a = b;
		} else if (Settings.getInstance().getDate_config() == 2) {
			fechaArray = c.split("/");
			dia = Integer.parseInt(fechaArray[0]);
			mes = Integer.parseInt(fechaArray[1]);
			anyo = Integer.parseInt(fechaArray[2]);
			b = new Fecha(anyo + "/" + mes + "/" + dia);
			a = b;
		} else if (Settings.getInstance().getDate_config() == 3) {
			fechaArray = c.split("/");
			dia = Integer.parseInt(fechaArray[0]);
			mes = Integer.parseInt(fechaArray[1]);
			anyo = Integer.parseInt(fechaArray[2]);
			b = new Fecha(anyo + "-" + mes + "-" + dia);
			a = b;
		}
		return a;
	}

	public static char askCharacter(String message, String title) {
		char neoChar = ' ';
		String string = "";
		boolean correct = true;

		do {
			try {
				string = JOptionPane.showInputDialog(null, message, title, JOptionPane.QUESTION_MESSAGE);
				neoChar = string.charAt(0);
				correct = true;

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, Language.getInstance().getErrorCharacter(), "Error",
						JOptionPane.ERROR_MESSAGE);
				correct = false;
			}
		} while (correct == false);

		return neoChar;
	}

	public static String askString(String message, String title) {
		String string = "";
		boolean a = false;

		do {
			try {
				string = JOptionPane.showInputDialog(null, message, title, JOptionPane.QUESTION_MESSAGE);
				a = true;
				if (string.equals("")) {
					JOptionPane.showMessageDialog(null, "Error", "Error", JOptionPane.ERROR_MESSAGE);
					a = false;
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, Language.getInstance().getChainError(), "Error",
						JOptionPane.ERROR_MESSAGE);
				a = false;
			}
		} while (a == false);

		return string;

	}

	public static int askinteger(String message, String title) {
		String string = "";
		int num = 0;
		boolean correct = false;

		do {
			try {
				string = JOptionPane.showInputDialog(null, message, title, JOptionPane.QUESTION_MESSAGE);
				num = Integer.parseInt(string);
				correct = true;
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, Language.getInstance().getNoNumber(), "Error",
						JOptionPane.ERROR_MESSAGE);
				correct = false;
			}
		} while (correct == false);

		return num;
	}

	public static int menubuttons(String[] options, String message, String title) {
		int option = 0;

		option = JOptionPane.showOptionDialog(null, message, title, JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

		return option;

	}

	public static void print(String message, String title) {
		JOptionPane.showMessageDialog(null, message, title, JOptionPane.INFORMATION_MESSAGE);
	}

}
