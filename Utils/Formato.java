package Framework_Sergio_Huertas_Gisbert.Utils;

//import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import Framework_Sergio_Huertas_Gisbert.Classes.*;
import Framework_Sergio_Huertas_Gisbert.Classes.Language;

public class Formato {
	public static String moneda = "�";
	public static int decimal = 2;
	public static int formato = 0;

	public static Float format1d(float number) {
		int a = (int) (number * 10);
		float b = a / 10.0f;
		return b;
		// DecimalFormat format1 = new DecimalFormat(".#");
		// return format1.format(number);
	}

	public static Float format2d(float number) {
		int a = (int) (number * 100);
		float b = a / 100.0f;
		return b;
		// DecimalFormat format1 = new DecimalFormat(".##");
		// return format1.format(number);
	}

	public static Float format3d(float number) {
		int a = (int) (number * 1000);
		float b = a / 1000.0f;
		return b;
		// DecimalFormat format1 = new DecimalFormat(".###");
		// return format1.format(number);
	}

	public static String FormatDollar(double moneda) {
		NumberFormat coin = NumberFormat.getCurrencyInstance(Locale.US);// Dolar
		return coin.format(moneda);
	}

	public static String FormatLibra(double moneda) {
		NumberFormat coin = NumberFormat.getCurrencyInstance(Locale.UK);// Libras
		return coin.format(moneda);
	}

	public static String FormatEuro(double moneda) {
		NumberFormat coin = NumberFormat.getCurrencyInstance(Locale.FRANCE);// Euro
		return coin.format(moneda);
	}

	public static void Formatomoneda() {
		int menu = 0;
		String[] option = { Language.getInstance().getDolar(), Language.getInstance().getLibra(), "Euro",
				Language.getInstance().getExit() };
		menu = F.menubuttons(option, Language.getInstance().getCurrency(), Language.getInstance().getCurrency());
		switch (menu) {
		case 0:
			moneda = "$";
			Settings.getInstance().setCurrency_config(moneda);
			break;
		case 1:
			moneda = "�";
			Settings.getInstance().setCurrency_config(moneda);
			break;
		case 2:
			moneda = "�";
			Settings.getInstance().setCurrency_config(moneda);
			break;
		case 3:
			break;
		}
	}

	public static void Formatdecimal() {
		int menu = 0;
		String[] option = { ".#", ".##", ".###", Language.getInstance().getExit() };
		menu = F.menubuttons(option, Language.getInstance().getDecimals(), Language.getInstance().getDecimals());
		switch (menu) {
		case 0:
			decimal = 1;
			Settings.getInstance().setDecimals_config(decimal);
			break;
		case 1:
			decimal = 2;
			Settings.getInstance().setDecimals_config(decimal);
			break;
		case 2:
			decimal = 3;
			Settings.getInstance().setDecimals_config(decimal);
			break;
		case 3:
			break;
		}
	}

	/*
	 * public static String ValorDecimal(int format, double moneda) { String
	 * decimales = ""; switch (format) { case 1: decimales =
	 * Formato.format1d(moneda); break; case 2: decimales =
	 * Formato.format2d(moneda); break; case 3: decimales =
	 * Formato.format3d(moneda); break; } return decimales; }
	 */

	public static void Formatfecha() {
		int menu = 0;
		String[] option = { "dd/mm/yyyy", "dd-mm-yyyy", "yyyy/mm/dd", "yyyy-mm-dd" };
		menu = F.menubuttons(option, Language.getInstance().getFecha(), Language.getInstance().getFecha());
		switch (menu) {
		case 0:
			formato = 0;
			Settings.getInstance().setDate_config(formato);
			break;
		case 1:
			formato = 1;
			Settings.getInstance().setDate_config(formato);
			break;
		case 2:
			formato = 2;
			Settings.getInstance().setDate_config(formato);
			break;
		case 3:
			formato = 3;
			Settings.getInstance().setDate_config(formato);
			break;
		}
	}

}
